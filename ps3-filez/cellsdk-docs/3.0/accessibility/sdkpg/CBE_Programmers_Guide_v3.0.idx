:BHXREF.HDR1cbe_programmers_guide_v3.0.htmwq3WQ3Contents"Contents"
:BHXREF.HDR2cbe_programmers_guide_v3.0.htmintroINTROPreface"Preface"
:BHXREF.HDR3cbe_programmers_guide_v3.0.htmwq4WQ4About this book"About this book"
:BHXREF.HDR4cbe_programmers_guide_v3.0.htmwq5WQ5What's new in this book"What's new in this book"
:BHXREF.HDR5cbe_programmers_guide_v3.0.htmwq6WQ6Supported platforms"Supported platforms"
:BHXREF.HDR6cbe_programmers_guide_v3.0.htmwq7WQ7Supported languages"Supported languages"
:BHXREF.HDR7cbe_programmers_guide_v3.0.htmwq9WQ9Beta-level (unsupported) environments"Beta-level (unsupported) environments"
:BHXREF.HDR8cbe_programmers_guide_v3.0.htmwq10WQ10Getting support"Getting support"
:BHXREF.HDR9cbe_programmers_guide_v3.0.htmwq11WQ11Related documentation"Related documentation"
:BHXREF.HDR10cbe_programmers_guide_v3.0.htmsdkcontSDKCONTSDK 3.0 overview"SDK 3.0 overview"
:BHXREF.HDR11cbe_programmers_guide_v3.0.htmgnutoolchainGNUTOOLCHAINGNU tool chain"GNU tool chain"
:BHXREF.HDR12cbe_programmers_guide_v3.0.htmxlcccompilerXLCCCOMPILERIBM XL C/C++ compiler"IBM XL C/C++ compiler"
:BHXREF.HDR13cbe_programmers_guide_v3.0.htmfullsyssimFULLSYSSIMIBM Full-System Simulator"IBM Full-System Simulator"
:BHXREF.HDR14cbe_programmers_guide_v3.0.htmrootsimROOTSIMSystem root image for the simulator"System root image for the simulator"
:BHXREF.HDR15cbe_programmers_guide_v3.0.htmlinuxkernelLINUXKERNELLinux kernel"Linux kernel"
:BHXREF.HDR16cbe_programmers_guide_v3.0.htmcellbelibCELLBELIBCell BE libraries"Cell BE libraries"
:BHXREF.HDR17cbe_programmers_guide_v3.0.htmlibspe2LIBSPE2SPE Runtime Management Library Version 2.2"SPE Runtime Management Library Version 2.2"
:BHXREF.HDR18cbe_programmers_guide_v3.0.htmsimdmathlibSIMDMATHLIBSIMD math libraries"SIMD math libraries"
:BHXREF.HDR19cbe_programmers_guide_v3.0.htmmasslibMASSLIBMathematical Acceleration Subsystem (MASS) libraries"Mathematical Acceleration Subsystem (MASS) libraries"
:BHXREF.HDR20cbe_programmers_guide_v3.0.htmalflibALFLIBALF library"ALF library"
:BHXREF.HDR21cbe_programmers_guide_v3.0.htmdacslibDACSLIBDaCS library"DaCS library"
:BHXREF.HDR22cbe_programmers_guide_v3.0.htmprotcodePROTCODEPrototype libraries"Prototype libraries"
:BHXREF.HDR23cbe_programmers_guide_v3.0.htmfftlibFFTLIBFast Fourier Transform (FFT) library"Fast Fourier Transform (FFT) library"
:BHXREF.HDR24cbe_programmers_guide_v3.0.htmmontelibMONTELIBMonte Carlo libraries"Monte Carlo libraries"
:BHXREF.HDR25cbe_programmers_guide_v3.0.htmwq13WQ13Code examples and example libraries"Code examples and example libraries"
:BHXREF.TBL1cbe_programmers_guide_v3.0.htmwq14WQ14Table 1. Subdirectories for the libraries and examples RPMTable 1
:BHXREF.HDR26cbe_programmers_guide_v3.0.htmlinlibLINLIBPerformance support libraries and utilities"Performance support libraries and utilities"
:BHXREF.HDR27cbe_programmers_guide_v3.0.htmwq18WQ18SPU timing tool"SPU timing tool"
:BHXREF.HDR28cbe_programmers_guide_v3.0.htmwq19WQ19OProfile"OProfile"
:BHXREF.HDR29cbe_programmers_guide_v3.0.htmwq22WQ22SPU profiling restrictions"SPU profiling restrictions"
:BHXREF.HDR30cbe_programmers_guide_v3.0.htmwq23WQ23SPU report anomalies"SPU report anomalies"
:BHXREF.HDR31cbe_programmers_guide_v3.0.htmwq25WQ25Cell-perf-counter tool"Cell-perf-counter tool"
:BHXREF.HDR32cbe_programmers_guide_v3.0.htmeclipseideECLIPSEIDEIBM Eclipse IDE for the SDK"IBM Eclipse IDE for the SDK"
:BHXREF.HDR33cbe_programmers_guide_v3.0.htmhybridoverviewHYBRIDOVERVIEWHybrid-x86 programming model overview"Hybrid-x86 programming model overview"
:BHXREF.HDR34cbe_programmers_guide_v3.0.htmsdkuseSDKUSEProgramming with the SDK"Programming with the SDK"
:BHXREF.HDR35cbe_programmers_guide_v3.0.htmsysrootdirSYSROOTDIRSystem root directories"System root directories"
:BHXREF.TBL2cbe_programmers_guide_v3.0.htmtablesysrootdirTABLESYSROOTDIRTable 2. System root directoriesTable 2
:BHXREF.HDR36cbe_programmers_guide_v3.0.htmrunsimRUNSIMRunning the simulator"Running the simulator"
:BHXREF.FIG1cbe_programmers_guide_v3.0.htmwq29WQ29Figure 1. Running the simulatorFigure 1
:BHXREF.HDR37cbe_programmers_guide_v3.0.htmcallthruCALLTHRUThe callthru utility"The callthru utility"
:BHXREF.HDR38cbe_programmers_guide_v3.0.htmwq36WQ36Read and write access to the simulator sysroot image"Read and write access to the simulator sysroot image"
:BHXREF.HDR39cbe_programmers_guide_v3.0.htmwq37WQ37Enabling Symmetric Multiprocessing support"Enabling Symmetric Multiprocessing support"
:BHXREF.HDR40cbe_programmers_guide_v3.0.htmwq38WQ38Enabling xclients from the simulator"Enabling xclients from the simulator"
:BHXREF.HDR41cbe_programmers_guide_v3.0.htmspecprocSPECPROCSpecifying the processor architecture"Specifying the processor architecture"
:BHXREF.TBL3cbe_programmers_guide_v3.0.htmwq42WQ42Table 3. spu-gcc compiler optionsTable 3
:BHXREF.TBL4cbe_programmers_guide_v3.0.htmwq45WQ45Table 4. spu-xlc compiler optionsTable 4
:BHXREF.HDR42cbe_programmers_guide_v3.0.htmppeaddressPPEADDRESSPPE address space support on SPE"PPE address space support on SPE"
:BHXREF.TBL5cbe_programmers_guide_v3.0.htmtabofoptsTABOFOPTSTable 5. OptionsTable 5
:BHXREF.HDR43cbe_programmers_guide_v3.0.htmprogexamplesPROGEXAMPLESSDK programming examples and demos"SDK programming examples and demos"
:BHXREF.HDR44cbe_programmers_guide_v3.0.htmwq50WQ50Overview of the build environment"Overview of the build environment"
:BHXREF.HDR45cbe_programmers_guide_v3.0.htmwq51WQ51Changing the default compiler"Changing the default compiler"
:BHXREF.HDR46cbe_programmers_guide_v3.0.htmwq52WQ52Building and running a specific program"Building and running a specific program"
:BHXREF.HDR47cbe_programmers_guide_v3.0.htmcomplinkgnuCOMPLINKGNUCompiling and linking with the GNU tool chain"Compiling and linking with the GNU tool chain"
:BHXREF.HDR48cbe_programmers_guide_v3.0.htmtlbTLBSupport for huge TLB file systems"Support for huge TLB file systems"
:BHXREF.HDR49cbe_programmers_guide_v3.0.htmbestBESTSDK development best practices"SDK development best practices"
:BHXREF.HDR50cbe_programmers_guide_v3.0.htmwq57WQ57Using a shared development environment"Using a shared development environment"
:BHXREF.HDR51cbe_programmers_guide_v3.0.htmperfcondPERFCONDPerformance considerations"Performance considerations"
:BHXREF.HDR52cbe_programmers_guide_v3.0.htmnumaNUMANUMA"NUMA"
:BHXREF.HDR53cbe_programmers_guide_v3.0.htmpreconswitchPRECONSWITCHPreemptive context switching"Preemptive context switching"
:BHXREF.HDR54cbe_programmers_guide_v3.0.htmsdkdebugSDKDEBUGDebugging Cell BE applications"Debugging Cell BE applications"
:BHXREF.HDR55cbe_programmers_guide_v3.0.htmdbugintroDBUGINTROOverview"Overview"
:BHXREF.HDR56cbe_programmers_guide_v3.0.htmwq58WQ58GDB for SDK 3.0"GDB for SDK 3.0"
:BHXREF.HDR57cbe_programmers_guide_v3.0.htmwq59WQ59Compiling with GCC or XLC"Compiling with GCC or XLC"
:BHXREF.HDR58cbe_programmers_guide_v3.0.htmusedebugUSEDEBUGUsing the debugger"Using the debugger"
:BHXREF.HDR59cbe_programmers_guide_v3.0.htmdbppecodeDBPPECODEDebugging PPE code"Debugging PPE code"
:BHXREF.HDR60cbe_programmers_guide_v3.0.htmdbspucodeDBSPUCODEDebugging SPE code"Debugging SPE code"
:BHXREF.HDR61cbe_programmers_guide_v3.0.htmwq62WQ62Source level debugging"Source level debugging"
:BHXREF.HDR62cbe_programmers_guide_v3.0.htmwq63WQ63Assembler level debugging"Assembler level debugging"
:BHXREF.HDR63cbe_programmers_guide_v3.0.htmwq64WQ64How spu-gdb manages SPE registers"How spu-gdb manages SPE registers"
:BHXREF.HDR64cbe_programmers_guide_v3.0.htmstackSTACKSPU stack analysis"SPU stack analysis"
:BHXREF.HDR65cbe_programmers_guide_v3.0.htmstackdebuggingSTACKDEBUGGINGSPE stack debugging"SPE stack debugging"
:BHXREF.HDR66cbe_programmers_guide_v3.0.htmwq65WQ65Overview"Overview"
:BHXREF.FIG2cbe_programmers_guide_v3.0.htmstackfigSTACKFIGFigure 2. SPE local storage and stack anatomyFigure 2
:BHXREF.HDR67cbe_programmers_guide_v3.0.htmwq66WQ66Stack overflow checking"Stack overflow checking"
:BHXREF.HDR68cbe_programmers_guide_v3.0.htmwq67WQ67Stack management strategies"Stack management strategies"
:BHXREF.HDR69cbe_programmers_guide_v3.0.htmdbcombineDBCOMBINEDebugging in the Cell BE environment"Debugging in the Cell BE environment"
:BHXREF.HDR70cbe_programmers_guide_v3.0.htmwq68WQ68Debugging multithreaded code"Debugging multithreaded code"
:BHXREF.HDR71cbe_programmers_guide_v3.0.htmdebugarchDEBUGARCHDebugging architecture"Debugging architecture"
:BHXREF.HDR72cbe_programmers_guide_v3.0.htmswitcharchSWITCHARCHSwitching architectures within a single thread"Switching architectures within a single thread"
:BHXREF.HDR73cbe_programmers_guide_v3.0.htmwq70WQ70Viewing symbolic and additional information"Viewing symbolic and additional information"
:BHXREF.HDR74cbe_programmers_guide_v3.0.htmwq71WQ71Using scheduler-locking"Using scheduler-locking"
:BHXREF.HDR75cbe_programmers_guide_v3.0.htmwq72WQ72Using the combined debugger"Using the combined debugger"
:BHXREF.HDR76cbe_programmers_guide_v3.0.htmpendingbptsPENDINGBPTSSetting pending breakpoints"Setting pending breakpoints"
:BHXREF.HDR77cbe_programmers_guide_v3.0.htmwq74WQ74Using the set spu stop-on-load command"Using the set spu stop-on-load command"
:BHXREF.HDR78cbe_programmers_guide_v3.0.htmdisambigDISAMBIGDisambiguation of multiply-defined global symbols"Disambiguation of multiply-defined global symbols"
:BHXREF.HDR79cbe_programmers_guide_v3.0.htmwq76WQ76New command reference"New command reference"
:BHXREF.HDR80cbe_programmers_guide_v3.0.htmwq78WQ78info spu event"info spu event"
:BHXREF.HDR81cbe_programmers_guide_v3.0.htmwq79WQ79info spu signal"info spu signal"
:BHXREF.HDR82cbe_programmers_guide_v3.0.htmwq80WQ80info spu mailbox"info spu mailbox"
:BHXREF.HDR83cbe_programmers_guide_v3.0.htmwq81WQ81info spu dma"info spu dma"
:BHXREF.HDR84cbe_programmers_guide_v3.0.htmwq82WQ82info spu proxydma"info spu proxydma"
:BHXREF.HDR85cbe_programmers_guide_v3.0.htmremdebugREMDEBUGSetting up remote debugging"Setting up remote debugging"
:BHXREF.HDR86cbe_programmers_guide_v3.0.htmremdboverviewREMDBOVERVIEWRemote debugging overview"Remote debugging overview"
:BHXREF.HDR87cbe_programmers_guide_v3.0.htmuseremdbUSEREMDBUsing remote debugging"Using remote debugging"
:BHXREF.HDR88cbe_programmers_guide_v3.0.htmstartremdbSTARTREMDBStarting remote debugging"Starting remote debugging"
:BHXREF.HDR89cbe_programmers_guide_v3.0.htmcbepdtCBEPDTCell BE Performance Debugging Tool"Cell BE Performance Debugging Tool"
:BHXREF.HDR90cbe_programmers_guide_v3.0.htmcbepdtintroCBEPDTINTROIntroduction"Introduction"
:BHXREF.HDR91cbe_programmers_guide_v3.0.htmwq90WQ90Components High Level Description"Components High Level Description"
:BHXREF.HDR92cbe_programmers_guide_v3.0.htmwq91WQ91Tracing Facility"Tracing Facility"
:BHXREF.HDR93cbe_programmers_guide_v3.0.htmwq93WQ93Trace Processing"Trace Processing"
:BHXREF.HDR94cbe_programmers_guide_v3.0.htmwq94WQ94Visualization"Visualization"
:BHXREF.FIG3cbe_programmers_guide_v3.0.htmtaguiTAGUIFigure 3. Trace Analyzer GUI on the FFT16M workloadFigure 3
:BHXREF.HDR95cbe_programmers_guide_v3.0.htmwq95WQ95Setting up the PDT tracing facility"Setting up the PDT tracing facility"
:BHXREF.TBL6cbe_programmers_guide_v3.0.htmopteronandcellOPTERONANDCELLTable 6. Tracing-facility directories on a Cell BE systemTable 6
:BHXREF.TBL7cbe_programmers_guide_v3.0.htmcrosssystemCROSSSYSTEMTable 7. Tracing-facility directories on a cross systemTable 7
:BHXREF.TBL8cbe_programmers_guide_v3.0.htmpdtaddfilePDTADDFILETable 8. Additional PDT filesTable 8
:BHXREF.HDR96cbe_programmers_guide_v3.0.htmwq102WQ102Configuring of the PDT kernel module"Configuring of the PDT kernel module"
:BHXREF.HDR97cbe_programmers_guide_v3.0.htmwq105WQ105PDT example usage"PDT example usage"
:BHXREF.HDR98cbe_programmers_guide_v3.0.htmwq106WQ106Enabling the PDT tracing facility for a new application"Enabling the PDT tracing facility for a new application"
:BHXREF.HDR99cbe_programmers_guide_v3.0.htmcompapbldCOMPAPBLDCompilation and application building"Compilation and application building"
:BHXREF.HDR100cbe_programmers_guide_v3.0.htmwq107WQ107SPE compilation"SPE compilation"
:BHXREF.HDR101cbe_programmers_guide_v3.0.htmwq109WQ109PPE compilation"PPE compilation"
:BHXREF.HDR102cbe_programmers_guide_v3.0.htmrunpgmRUNPGMRunning a trace-enabled program using the PDT libraries"Running a trace-enabled program using the PDT libraries"
:BHXREF.LI1cbe_programmers_guide_v3.0.htmenvvarENVVAR11
:BHXREF.TBL9cbe_programmers_guide_v3.0.htmwq112WQ112Table 9. Output directory filesTable 9
:BHXREF.HDR103cbe_programmers_guide_v3.0.htmwq117WQ117Running a program using SPE profiling"Running a program using SPE profiling"
:BHXREF.HDR104cbe_programmers_guide_v3.0.htmconfigpdtCONFIGPDTConfiguring the PDT for an application run"Configuring the PDT for an application run"
:BHXREF.HDR105cbe_programmers_guide_v3.0.htmwq120WQ120Using the Tracing API"Using the Tracing API"
:BHXREF.HDR106cbe_programmers_guide_v3.0.htmessdefESSDEFEssential definitions"Essential definitions"
:BHXREF.HDR107cbe_programmers_guide_v3.0.htmwq121WQ121Application programmer API"Application programmer API"
:BHXREF.HDR108cbe_programmers_guide_v3.0.htmwq122WQ122User-defined events"User-defined events"
:BHXREF.HDR109cbe_programmers_guide_v3.0.htmwq123WQ123Dynamic trace control"Dynamic trace control"
:BHXREF.HDR110cbe_programmers_guide_v3.0.htmwq124WQ124Library developer API"Library developer API"
:BHXREF.HDR111cbe_programmers_guide_v3.0.htmtracefcTRACEFCTrace facility control"Trace facility control"
:BHXREF.HDR112cbe_programmers_guide_v3.0.htmwq125WQ125Events recording"Events recording"
:BHXREF.HDR113cbe_programmers_guide_v3.0.htmwq127WQ127Restrictions"Restrictions"
:BHXREF.HDR114cbe_programmers_guide_v3.0.htmwq128WQ128Installing and using the PDT trace facility on Hybrid-x86"Installing and using the PDT trace facility on Hybrid-x86"
:BHXREF.TBL10cbe_programmers_guide_v3.0.htmopteroncellOPTERONCELLTable 10. Tracing-facility directories on Hybrid-x86Table 10
:BHXREF.HDR115cbe_programmers_guide_v3.0.htmwq131WQ131PDT on Hybrid-x86 example usage"PDT on Hybrid-x86 example usage"
:BHXREF.HDR116cbe_programmers_guide_v3.0.htmwq133WQ133Using the PDTR tool (pdtr command)"Using the PDTR tool (pdtr command)"
:BHXREF.HDR117cbe_programmers_guide_v3.0.htmcrashdumpCRASHDUMPAnalyzing Cell BE SPUs with kdump and crash"Analyzing Cell BE SPUs with kdump and crash"
:BHXREF.HDR118cbe_programmers_guide_v3.0.htmwq136WQ136Installation requirements"Installation requirements"
:BHXREF.HDR119cbe_programmers_guide_v3.0.htmwq137WQ137Production system"Production system"
:BHXREF.HDR120cbe_programmers_guide_v3.0.htmwq139WQ139Analysis system"Analysis system"
:BHXREF.HDR121cbe_programmers_guide_v3.0.htmfdprproFDPRPROFeedback Directed Program Restructuring (FDPR-Pro)"Feedback Directed Program Restructuring (FDPR-Pro)"
:BHXREF.HDR122cbe_programmers_guide_v3.0.htmwq141WQ141Introduction"Introduction"
:BHXREF.HDR123cbe_programmers_guide_v3.0.htminputfileINPUTFILEInput files"Input files"
:BHXREF.HDR124cbe_programmers_guide_v3.0.htminstrumentationINSTRUMENTATIONInstrumentation and profiling"Instrumentation and profiling"
:BHXREF.HDR125cbe_programmers_guide_v3.0.htmoptimizationOPTIMIZATIONOptimizations"Optimizations"
:BHXREF.HDR126cbe_programmers_guide_v3.0.htmwq143WQ143Instrumentation and optimization options"Instrumentation and optimization options"
:BHXREF.HDR127cbe_programmers_guide_v3.0.htmprofspePROFSPEProfiling SPE executable files"Profiling SPE executable files"
:BHXREF.HDR128cbe_programmers_guide_v3.0.htmprocessingPROCESSINGProcessing PPE/SPE executable files"Processing PPE/SPE executable files"
:BHXREF.HDR129cbe_programmers_guide_v3.0.htmwq145WQ145Integrated mode"Integrated mode"
:BHXREF.HDR130cbe_programmers_guide_v3.0.htmwq146WQ146Standalone mode"Standalone mode"
:BHXREF.HDR131cbe_programmers_guide_v3.0.htmhumanHUMANHuman-readable output"Human-readable output"
:BHXREF.HDR132cbe_programmers_guide_v3.0.htmideandfdprIDEANDFDPRRunning fdprpro from the IDE"Running fdprpro from the IDE"
:BHXREF.HDR133cbe_programmers_guide_v3.0.htmxdevfdprXDEVFDPRCross-development with FDPR-Pro"Cross-development with FDPR-Pro"
:BHXREF.HDR134cbe_programmers_guide_v3.0.htmsdkoverlaySDKOVERLAY<span>SPU code overlays</span>"<span>SPU code overlays</span>"
:BHXREF.HDR135cbe_programmers_guide_v3.0.htmwq149WQ149What are overlays"What are overlays"
:BHXREF.HDR136cbe_programmers_guide_v3.0.htmwq150WQ150How overlays work"How overlays work"
:BHXREF.HDR137cbe_programmers_guide_v3.0.htmwq151WQ151Restrictions on the use of overlays"Restrictions on the use of overlays"
:BHXREF.HDR138cbe_programmers_guide_v3.0.htmwq152WQ152<span>Planning to use overlays</span>"<span>Planning to use overlays</span>"
:BHXREF.HDR139cbe_programmers_guide_v3.0.htmwq153WQ153<span>Overview</span>"<span>Overview</span>"
:BHXREF.HDR140cbe_programmers_guide_v3.0.htmwq154WQ154Sizing"Sizing"
:BHXREF.HDR141cbe_programmers_guide_v3.0.htmscalingconsidsSCALINGCONSIDSScaling considerations"Scaling considerations"
:BHXREF.HDR142cbe_programmers_guide_v3.0.htmwq155WQ155<span>Overlay tree structure example</span>"<span>Overlay tree structure example</span>"
:BHXREF.FIG4cbe_programmers_guide_v3.0.htmfig02FIG02Figure 4. Overlay tree structureFigure 4
:BHXREF.HDR143cbe_programmers_guide_v3.0.htmwq156WQ156Length of an overlay program"Length of an overlay program"
:BHXREF.TBL11cbe_programmers_guide_v3.0.htmwq157WQ157Table 11. example program lengthsTable 11
:BHXREF.FIG5cbe_programmers_guide_v3.0.htmfig03FIG03Figure 5. Length of an overlay moduleFigure 5
:BHXREF.HDR144cbe_programmers_guide_v3.0.htmwq161WQ161Segment origin"Segment origin"
:BHXREF.TBL12cbe_programmers_guide_v3.0.htmwq162WQ162Table 12. Segment originsTable 12
:BHXREF.FIG6cbe_programmers_guide_v3.0.htmfig04FIG04Figure 6. Segment origin and use of storageFigure 6
:BHXREF.HDR145cbe_programmers_guide_v3.0.htmoverlayprocessingOVERLAYPROCESSINGOverlay processing"Overlay processing"
:BHXREF.FIG7cbe_programmers_guide_v3.0.htmfig07FIG07Figure 7. Location of stubs and tables in an overlay programFigure 7
:BHXREF.HDR146cbe_programmers_guide_v3.0.htmwq165WQ165Call stubs"Call stubs"
:BHXREF.HDR147cbe_programmers_guide_v3.0.htmwq166WQ166Segment and region tables"Segment and region tables"
:BHXREF.HDR148cbe_programmers_guide_v3.0.htmogseOGSEOverlay graph structure example"Overlay graph structure example"
:BHXREF.FIG8cbe_programmers_guide_v3.0.htmfig08FIG08Figure 8. Overlay graph structureFigure 8
:BHXREF.FIG9cbe_programmers_guide_v3.0.htmfig09FIG09Figure 9. Overlay graph using multiple regionsFigure 9
:BHXREF.FIG10cbe_programmers_guide_v3.0.htmfig10FIG10Figure 10. Overlay graph segment origin and use of storageFigure 10
:BHXREF.HDR149cbe_programmers_guide_v3.0.htmwq167WQ167Specification of an SPU overlay program"Specification of an SPU overlay program"
:BHXREF.HDR150cbe_programmers_guide_v3.0.htmwq168WQ168""
:BHXREF.HDR151cbe_programmers_guide_v3.0.htmwq169WQ169Coding for overlays"Coding for overlays"
:BHXREF.HDR152cbe_programmers_guide_v3.0.htmwq170WQ170Migration/Co-Existence/Binary-Compatibility Considerations"Migration/Co-Existence/Binary-Compatibility Considerations"
:BHXREF.HDR153cbe_programmers_guide_v3.0.htmwq171WQ171Compiler options (XLC only)"Compiler options (XLC only)"
:BHXREF.TBL13cbe_programmers_guide_v3.0.htmwq173WQ173Table 13. Compiler optionsTable 13
:BHXREF.HDR154cbe_programmers_guide_v3.0.htmwq176WQ176<span>SDK overlay examples</span>"<span>SDK overlay examples</span>"
:BHXREF.HDR155cbe_programmers_guide_v3.0.htmsopcdSOPCDSimple overlay example"Simple overlay example"
:BHXREF.FIG11cbe_programmers_guide_v3.0.htmfig13FIG13Figure 11. Simple overlay program call graphFigure 11
:BHXREF.FIG12cbe_programmers_guide_v3.0.htmfig14FIG14Figure 12. Simple overlay program regions, segments and sectionsFigure 12
:BHXREF.FIG13cbe_programmers_guide_v3.0.htmfig15FIG15Figure 13. Simple overlay program logical structureFigure 13
:BHXREF.FIG14cbe_programmers_guide_v3.0.htmfig16FIG16Figure 14. Simple overlay program physical structureFigure 14
:BHXREF.FIG15cbe_programmers_guide_v3.0.htmfig17FIG17Figure 15. Example overlay program interaction diagramFigure 15
:BHXREF.HDR156cbe_programmers_guide_v3.0.htmoopcdOOPCDOverview overlay example"Overview overlay example"
:BHXREF.HDR157cbe_programmers_guide_v3.0.htmlmopcdLMOPCDLarge matrix overlay example"Large matrix overlay example"
:BHXREF.FIG16cbe_programmers_guide_v3.0.htmfig19FIG19Figure 16. Large matrix overlay program call graphFigure 16
:BHXREF.FIG17cbe_programmers_guide_v3.0.htmfig20FIG20Figure 17. Large matrix program physical structureFigure 17
:BHXREF.HDR158cbe_programmers_guide_v3.0.htmgnulinkdetailsGNULINKDETAILS<span>Using the GNU SPU linker for overlays</span>"<span>Using the GNU SPU linker for overlays</span>"
:BHXREF.HDR159cbe_programmers_guide_v3.0.htmsdkchangesSDKCHANGESAppendix A. Changes to SDK for this release"Appendix A. Changes to SDK for this release"
:BHXREF.HDR160cbe_programmers_guide_v3.0.htmwq181WQ181Changes to the directory structure"Changes to the directory structure"
:BHXREF.HDR161cbe_programmers_guide_v3.0.htmwq182WQ182Selecting the compiler"Selecting the compiler"
:BHXREF.HDR162cbe_programmers_guide_v3.0.htmwq183WQ183Synching code into the simulator sysroot image"Synching code into the simulator sysroot image"
:BHXREF.HDR163cbe_programmers_guide_v3.0.htmknownprobKNOWNPROBAppendix B. PDT troubleshooting"Appendix B. PDT troubleshooting"
:BHXREF.HDR164cbe_programmers_guide_v3.0.htmdqx1reldocDQX1RELDOCAppendix C. Related documentation"Appendix C. Related documentation"
:BHXREF.HDR165cbe_programmers_guide_v3.0.htmwq185WQ185Document location"Document location"
:BHXREF.HDR166cbe_programmers_guide_v3.0.htmwq186WQ186Architecture"Architecture"
:BHXREF.HDR167cbe_programmers_guide_v3.0.htmwq187WQ187Standards"Standards"
:BHXREF.HDR168cbe_programmers_guide_v3.0.htmwq188WQ188Programming"Programming"
:BHXREF.HDR169cbe_programmers_guide_v3.0.htmwq189WQ189Library"Library"
:BHXREF.HDR170cbe_programmers_guide_v3.0.htmwq190WQ190Installation"Installation"
:BHXREF.HDR171cbe_programmers_guide_v3.0.htmwq191WQ191Tools"Tools"
:BHXREF.HDR172cbe_programmers_guide_v3.0.htmwq192WQ192PowerPC Base"PowerPC Base"
:BHXREF.HDR173cbe_programmers_guide_v3.0.htmservernoticesSERVERNOTICESAppendix D. Notices"Appendix D. Notices"
:BHXREF.HDR174cbe_programmers_guide_v3.0.htmwq193WQ193Trademarks"Trademarks"
:BHXREF.HDR175cbe_programmers_guide_v3.0.htmwq195WQ195Glossary"Glossary"
:BHXREF.HDR176cbe_programmers_guide_v3.0.htmdqx2glossaryDQX2GLOSSARYGlossary"Glossary"
:BHXREF.HDR177cbe_programmers_guide_v3.0.htmdqx2abiDQX2ABIABI"ABI"
:BHXREF.HDR178cbe_programmers_guide_v3.0.htmdqx2alfDQX2ALFALF"ALF"
:BHXREF.HDR179cbe_programmers_guide_v3.0.htmdqx2apiDQX2APIAPI"API"
:BHXREF.HDR180cbe_programmers_guide_v3.0.htmdqx2atomicoperationDQX2ATOMICOPERATIONatomic operation"atomic operation"
:BHXREF.HDR181cbe_programmers_guide_v3.0.htmdqx2auto-simdizeDQX2AUTO-SIMDIZEAuto-SIMDize"Auto-SIMDize"
:BHXREF.HDR182cbe_programmers_guide_v3.0.htmdqx2barcelonasupercomputingcenterDQX2BARCELONASUPERCOMPUTINGCENTERBarcelona Supercomputing Center"Barcelona Supercomputing Center"
:BHXREF.HDR183cbe_programmers_guide_v3.0.htmdqx2beDQX2BEBE"BE"
:BHXREF.HDR184cbe_programmers_guide_v3.0.htmdqx2broadbandengineDQX2BROADBANDENGINEBroadband Engine"Broadband Engine"
:BHXREF.HDR185cbe_programmers_guide_v3.0.htmdqx2bscDQX2BSCBSC"BSC"
:BHXREF.HDR186cbe_programmers_guide_v3.0.htmdqx2cplusplusDQX2CPLUSPLUSC++"C++"
:BHXREF.HDR187cbe_programmers_guide_v3.0.htmdqx2cacheDQX2CACHEcache"cache"
:BHXREF.HDR188cbe_programmers_guide_v3.0.htmdqx2callstubDQX2CALLSTUBcall stub"call stub"
:BHXREF.HDR189cbe_programmers_guide_v3.0.htmdqx2cellbeDQX2CELLBECell BE processor"Cell BE processor"
:BHXREF.HDR190cbe_programmers_guide_v3.0.htmdqx2cbeaDQX2CBEACBEA"CBEA"
:BHXREF.HDR191cbe_programmers_guide_v3.0.htmdqx2cellbroadbandengineprocessorDQX2CELLBROADBANDENGINEPROCESSORCell Broadband Engine processor"Cell Broadband Engine processor"
:BHXREF.HDR192cbe_programmers_guide_v3.0.htmdqx2codesectionDQX2CODESECTIONcode section"code section"
:BHXREF.HDR193cbe_programmers_guide_v3.0.htmdqx2coherenceDQX2COHERENCEcoherence"coherence"
:BHXREF.HDR194cbe_programmers_guide_v3.0.htmdqx2compilerDQX2COMPILERcompiler"compiler"
:BHXREF.HDR195cbe_programmers_guide_v3.0.htmdqx2computationalkernelglossDQX2COMPUTATIONALKERNELGLOSScomputational kernel"computational kernel"
:BHXREF.HDR196cbe_programmers_guide_v3.0.htmdqx2computetaskglossDQX2COMPUTETASKGLOSScompute task"compute task"
:BHXREF.HDR197cbe_programmers_guide_v3.0.htmdqx2cpcDQX2CPCCPC"CPC"
:BHXREF.HDR198cbe_programmers_guide_v3.0.htmdqx2cpiDQX2CPICPI"CPI"
:BHXREF.HDR199cbe_programmers_guide_v3.0.htmdqx2cplDQX2CPLCPL"CPL"
:BHXREF.HDR200cbe_programmers_guide_v3.0.htmdqx2cycleDQX2CYCLEcycle"cycle"
:BHXREF.HDR201cbe_programmers_guide_v3.0.htmdqx2cycle-accuratesimulationDQX2CYCLE-ACCURATESIMULATIONCycle-accurate simulation"Cycle-accurate simulation"
:BHXREF.HDR202cbe_programmers_guide_v3.0.htmdqx2dacsglossDQX2DACSGLOSSDaCS"DaCS"
:BHXREF.HDR203cbe_programmers_guide_v3.0.htmdqx2dacselementDQX2DACSELEMENTDaCS Element"DaCS Element"
:BHXREF.HDR204cbe_programmers_guide_v3.0.htmdqx2deDQX2DEDE"DE"
:BHXREF.HDR205cbe_programmers_guide_v3.0.htmdqx2dmaDQX2DMADMA"DMA"
:BHXREF.HDR206cbe_programmers_guide_v3.0.htmdqx2dmacommandDQX2DMACOMMANDDMA command"DMA command"
:BHXREF.HDR207cbe_programmers_guide_v3.0.htmdqx2dmalistDQX2DMALISTDMA list"DMA list"
:BHXREF.HDR208cbe_programmers_guide_v3.0.htmdqx2dual-issueDQX2DUAL-ISSUEdual-issue"dual-issue"
:BHXREF.HDR209cbe_programmers_guide_v3.0.htmdqx2eaDQX2EAEA"EA"
:BHXREF.HDR210cbe_programmers_guide_v3.0.htmdqx2eccDQX2ECCECC"ECC"
:BHXREF.HDR211cbe_programmers_guide_v3.0.htmdqx2effectiveaddressDQX2EFFECTIVEADDRESSeffective address"effective address"
:BHXREF.HDR212cbe_programmers_guide_v3.0.htmdqx2elfDQX2ELFELF"ELF"
:BHXREF.HDR213cbe_programmers_guide_v3.0.htmdqx2elfspeDQX2ELFSPEelfspe"elfspe"
:BHXREF.HDR214cbe_programmers_guide_v3.0.htmdqx2ext3DQX2EXT3ext3"ext3"
:BHXREF.HDR215cbe_programmers_guide_v3.0.htmdqx2fdprproDQX2FDPRPROFDPR-Pro"FDPR-Pro"
:BHXREF.HDR216cbe_programmers_guide_v3.0.htmdqx2fedoraDQX2FEDORAFedora"Fedora"
:BHXREF.HDR217cbe_programmers_guide_v3.0.htmdqx2fenceDQX2FENCEfence"fence"
:BHXREF.HDR218cbe_programmers_guide_v3.0.htmdqx2fftDQX2FFTFFT"FFT"
:BHXREF.HDR219cbe_programmers_guide_v3.0.htmdqx2firmwareDQX2FIRMWAREfirmware"firmware"
:BHXREF.HDR220cbe_programmers_guide_v3.0.htmdqx2fsfDQX2FSFFSF"FSF"
:BHXREF.HDR221cbe_programmers_guide_v3.0.htmdqx2fssDQX2FSSFSS"FSS"
:BHXREF.HDR222cbe_programmers_guide_v3.0.htmdqx2gccDQX2GCCGCC"GCC"
:BHXREF.HDR223cbe_programmers_guide_v3.0.htmdqx2gdbDQX2GDBGDB"GDB"
:BHXREF.HDR224cbe_programmers_guide_v3.0.htmdqx2gnuDQX2GNUGNU"GNU"
:BHXREF.HDR225cbe_programmers_guide_v3.0.htmdqx2gplDQX2GPLGPL"GPL"
:BHXREF.HDR226cbe_programmers_guide_v3.0.htmdqx2graphstructureDQX2GRAPHSTRUCTUREgraph structure"graph structure"
:BHXREF.HDR227cbe_programmers_guide_v3.0.htmdqx2groupDQX2GROUPgroup"group"
:BHXREF.HDR228cbe_programmers_guide_v3.0.htmdqx2guardedDQX2GUARDEDguarded"guarded"
:BHXREF.HDR229cbe_programmers_guide_v3.0.htmdqx2guiDQX2GUIGUI"GUI"
:BHXREF.HDR230cbe_programmers_guide_v3.0.htmdqx2handleDQX2HANDLEhandle"handle"
:BHXREF.HDR231cbe_programmers_guide_v3.0.htmdqx2hostglossDQX2HOSTGLOSShost"host"
:BHXREF.HDR232cbe_programmers_guide_v3.0.htmdqx2httpDQX2HTTPHTTP"HTTP"
:BHXREF.HDR233cbe_programmers_guide_v3.0.htmdqx2hybridglossDQX2HYBRIDGLOSSHybrid"Hybrid"
:BHXREF.HDR234cbe_programmers_guide_v3.0.htmdqx2ideDQX2IDEIDE"IDE"
:BHXREF.HDR235cbe_programmers_guide_v3.0.htmdqx2idlDQX2IDLIDL"IDL"
:BHXREF.HDR236cbe_programmers_guide_v3.0.htmdqx2ilarDQX2ILARILAR"ILAR"
:BHXREF.HDR237cbe_programmers_guide_v3.0.htmdqx2initrdDQX2INITRDinitrd"initrd"
:BHXREF.HDR238cbe_programmers_guide_v3.0.htmdqx2interruptDQX2INTERRUPTinterrupt"interrupt"
:BHXREF.HDR239cbe_programmers_guide_v3.0.htmdqx2intrinsicDQX2INTRINSICintrinsic"intrinsic"
:BHXREF.HDR240cbe_programmers_guide_v3.0.htmdqx2isoimageDQX2ISOIMAGEISO image"ISO image"
:BHXREF.HDR241cbe_programmers_guide_v3.0.htmdqx2kandrprogrammingDQX2KANDRPROGRAMMINGK&amp;R programming"K&amp;R programming"
:BHXREF.HDR242cbe_programmers_guide_v3.0.htmdqx2kernelDQX2KERNELkernel"kernel"
:BHXREF.HDR243cbe_programmers_guide_v3.0.htmdqx2l1DQX2L1L1"L1"
:BHXREF.HDR244cbe_programmers_guide_v3.0.htmdqx2l2DQX2L2L2"L2"
:BHXREF.HDR245cbe_programmers_guide_v3.0.htmdqx2laDQX2LALA"LA"
:BHXREF.HDR246cbe_programmers_guide_v3.0.htmdqx2latencyDQX2LATENCYlatency"latency"
:BHXREF.HDR247cbe_programmers_guide_v3.0.htmdqx2lgplDQX2LGPLLGPL"LGPL"
:BHXREF.HDR248cbe_programmers_guide_v3.0.htmdqx2libspeDQX2LIBSPElibspe"libspe"
:BHXREF.HDR249cbe_programmers_guide_v3.0.htmdqx2listelementDQX2LISTELEMENTlist element"list element"
:BHXREF.HDR250cbe_programmers_guide_v3.0.htmdqx2lnopDQX2LNOPlnop"lnop"
:BHXREF.HDR251cbe_programmers_guide_v3.0.htmdqx2loopunrollingDQX2LOOPUNROLLINGloop unrolling"loop unrolling"
:BHXREF.HDR252cbe_programmers_guide_v3.0.htmdqx2lsDQX2LSLS"LS"
:BHXREF.HDR253cbe_programmers_guide_v3.0.htmdqx2lsaDQX2LSALSA"LSA"
:BHXREF.HDR254cbe_programmers_guide_v3.0.htmdqx2mainmemoryDQX2MAINMEMORYmain memory"main memory"
:BHXREF.HDR255cbe_programmers_guide_v3.0.htmdqx2mainstorageDQX2MAINSTORAGEmain storage"main storage"
:BHXREF.HDR256cbe_programmers_guide_v3.0.htmdqx2makefileDQX2MAKEFILEMakefile"Makefile"
:BHXREF.HDR257cbe_programmers_guide_v3.0.htmdqx2mailboxDQX2MAILBOXmailbox"mailbox"
:BHXREF.HDR258cbe_programmers_guide_v3.0.htmdqx2mainthreadDQX2MAINTHREADmain thread"main thread"
:BHXREF.HDR259cbe_programmers_guide_v3.0.htmdqx2mamboDQX2MAMBOMambo"Mambo"
:BHXREF.HDR260cbe_programmers_guide_v3.0.htmdqx2massDQX2MASSMASS"MASS"
:BHXREF.HDR261cbe_programmers_guide_v3.0.htmdqx2mfcDQX2MFCMFC"MFC"
:BHXREF.HDR262cbe_programmers_guide_v3.0.htmdqx2mfcproxycommandsDQX2MFCPROXYCOMMANDSMFC proxy commands"MFC proxy commands"
:BHXREF.HDR263cbe_programmers_guide_v3.0.htmdqx2smpdglossDQX2SMPDGLOSSMPMD"MPMD"
:BHXREF.HDR264cbe_programmers_guide_v3.0.htmdqx2mtDQX2MTMT"MT"
:BHXREF.HDR265cbe_programmers_guide_v3.0.htmdqx2multithreadingDQX2MULTITHREADINGmultithreading"multithreading"
:BHXREF.HDR266cbe_programmers_guide_v3.0.htmdqx2nanDQX2NANNaN"NaN"
:BHXREF.HDR267cbe_programmers_guide_v3.0.htmdqx2netbootDQX2NETBOOTnetboot"netboot"
:BHXREF.HDR268cbe_programmers_guide_v3.0.htmdqx2nodeDQX2NODEnode"node"
:BHXREF.HDR269cbe_programmers_guide_v3.0.htmdqx2numaDQX2NUMANUMA"NUMA"
:BHXREF.HDR270cbe_programmers_guide_v3.0.htmdqx2oprofileDQX2OPROFILEOprofile"Oprofile"
:BHXREF.HDR271cbe_programmers_guide_v3.0.htmdqx2overlayregionDQX2OVERLAYREGIONoverlay region"overlay region"
:BHXREF.HDR272cbe_programmers_guide_v3.0.htmdqx2overlayDQX2OVERLAYoverlay"overlay"
:BHXREF.HDR273cbe_programmers_guide_v3.0.htmdqx2pagetableDQX2PAGETABLEpage table"page table"
:BHXREF.HDR274cbe_programmers_guide_v3.0.htmdqx2parentDQX2PARENTparent"parent"
:BHXREF.HDR275cbe_programmers_guide_v3.0.htmdqx2pdfDQX2PDFPDF"PDF"
:BHXREF.HDR276cbe_programmers_guide_v3.0.htmdqx2performancesimulationDQX2PERFORMANCESIMULATIONPerformance simulation"Performance simulation"
:BHXREF.HDR277cbe_programmers_guide_v3.0.htmdqx2perlDQX2PERLPERL"PERL"
:BHXREF.HDR278cbe_programmers_guide_v3.0.htmdqx2pipeliningDQX2PIPELININGpipelining"pipelining"
:BHXREF.HDR279cbe_programmers_guide_v3.0.htmdqx2pluginDQX2PLUGINplugin"plugin"
:BHXREF.HDR280cbe_programmers_guide_v3.0.htmdqx2ppc-64DQX2PPC-64PPC-64"PPC-64"
:BHXREF.HDR281cbe_programmers_guide_v3.0.htmdqx2ppcDQX2PPCPPC"PPC"
:BHXREF.HDR282cbe_programmers_guide_v3.0.htmdqx2ppeDQX2PPEPPE"PPE"
:BHXREF.HDR283cbe_programmers_guide_v3.0.htmdqx2ppssDQX2PPSSPPSS"PPSS"
:BHXREF.HDR284cbe_programmers_guide_v3.0.htmdqx2ppuDQX2PPUPPU"PPU"
:BHXREF.HDR285cbe_programmers_guide_v3.0.htmdqx2programsectionDQX2PROGRAMSECTIONprogram section"program section"
:BHXREF.HDR286cbe_programmers_guide_v3.0.htmdqx2proxyDQX2PROXYproxy"proxy"
:BHXREF.HDR287cbe_programmers_guide_v3.0.htmdqx2regionDQX2REGIONregion"region"
:BHXREF.HDR288cbe_programmers_guide_v3.0.htmdqx2rootsegmentDQX2ROOTSEGMENTroot segment"root segment"
:BHXREF.HDR289cbe_programmers_guide_v3.0.htmdqx2rpmDQX2RPMRPM"RPM"
:BHXREF.HDR290cbe_programmers_guide_v3.0.htmdqx2sandboxDQX2SANDBOXSandbox"Sandbox"
:BHXREF.HDR291cbe_programmers_guide_v3.0.htmdqx2sdkDQX2SDKSDK"SDK"
:BHXREF.HDR292cbe_programmers_guide_v3.0.htmdqx2sectionDQX2SECTIONsection"section"
:BHXREF.HDR293cbe_programmers_guide_v3.0.htmdqx2segmentDQX2SEGMENTsegment"segment"
:BHXREF.HDR294cbe_programmers_guide_v3.0.htmdqx2sfpDQX2SFPSFP"SFP"
:BHXREF.HDR295cbe_programmers_guide_v3.0.htmdqx2signalDQX2SIGNALsignal"signal"
:BHXREF.HDR296cbe_programmers_guide_v3.0.htmdqx2signalnotificationDQX2SIGNALNOTIFICATIONsignal notification"signal notification"
:BHXREF.HDR297cbe_programmers_guide_v3.0.htmdqx2simdDQX2SIMDSIMD"SIMD"
:BHXREF.HDR298cbe_programmers_guide_v3.0.htmdqx2simdizeDQX2SIMDIZESIMDize"SIMDize"
:BHXREF.HDR299cbe_programmers_guide_v3.0.htmdqx2smpDQX2SMPSMP"SMP"
:BHXREF.HDR300cbe_programmers_guide_v3.0.htmdqx2speDQX2SPESPE"SPE"
:BHXREF.HDR301cbe_programmers_guide_v3.0.htmdqx2spethreadDQX2SPETHREADSPE thread"SPE thread"
:BHXREF.HDR302cbe_programmers_guide_v3.0.htmdqx2specificintrinsicDQX2SPECIFICINTRINSICspecific intrinsic"specific intrinsic"
:BHXREF.HDR303cbe_programmers_guide_v3.0.htmdqx2splatDQX2SPLATsplat"splat"
:BHXREF.HDR304cbe_programmers_guide_v3.0.htmdqx2dt2mg3DQX2DT2MG3SPMD"SPMD"
:BHXREF.HDR305cbe_programmers_guide_v3.0.htmdqx2spuDQX2SPUSPU"SPU"
:BHXREF.HDR306cbe_programmers_guide_v3.0.htmdqx2spuletDQX2SPULETspulet"spulet"
:BHXREF.HDR307cbe_programmers_guide_v3.0.htmdqx2stubDQX2STUBstub"stub"
:BHXREF.HDR308cbe_programmers_guide_v3.0.htmdqx2synchronizationDQX2SYNCHRONIZATIONsynchronization"synchronization"
:BHXREF.HDR309cbe_programmers_guide_v3.0.htmdqx2systemxDQX2SYSTEMXSystem X"System X"
:BHXREF.HDR310cbe_programmers_guide_v3.0.htmdqx2taggroupDQX2TAGGROUPtag group"tag group"
:BHXREF.HDR311cbe_programmers_guide_v3.0.htmdqx2tclDQX2TCLTcl"Tcl"
:BHXREF.HDR312cbe_programmers_guide_v3.0.htmdqx2tftpDQX2TFTPTFTP"TFTP"
:BHXREF.HDR313cbe_programmers_guide_v3.0.htmdqx2threadDQX2THREADthread"thread"
:BHXREF.HDR314cbe_programmers_guide_v3.0.htmdqx2tlbDQX2TLBTLB"TLB"
:BHXREF.HDR315cbe_programmers_guide_v3.0.htmdqx2treestructureDQX2TREESTRUCTUREtree structure"tree structure"
:BHXREF.HDR316cbe_programmers_guide_v3.0.htmdqx2tsDQX2TSTS"TS"
:BHXREF.HDR317cbe_programmers_guide_v3.0.htmdqx2udpDQX2UDPUDP"UDP"
:BHXREF.HDR318cbe_programmers_guide_v3.0.htmdqx2usermodeDQX2USERMODEuser mode"user mode"
:BHXREF.HDR319cbe_programmers_guide_v3.0.htmdqx2vectorDQX2VECTORvector"vector"
:BHXREF.HDR320cbe_programmers_guide_v3.0.htmdqx2virtualmemoryDQX2VIRTUALMEMORYvirtual memory"virtual memory"
:BHXREF.HDR321cbe_programmers_guide_v3.0.htmdqx2virtualstorageDQX2VIRTUALSTORAGEvirtual storage"virtual storage"
:BHXREF.HDR322cbe_programmers_guide_v3.0.htmdqx2vmaDQX2VMAVMA"VMA"
:BHXREF.HDR323cbe_programmers_guide_v3.0.htmdqx2workblockglossDQX2WORKBLOCKGLOSSwork block"work block"
:BHXREF.HDR324cbe_programmers_guide_v3.0.htmdqx2workloadDQX2WORKLOADworkload"workload"
:BHXREF.HDR325cbe_programmers_guide_v3.0.htmdqx2workqueueglossDQX2WORKQUEUEGLOSSwork queue"work queue"
:BHXREF.HDR326cbe_programmers_guide_v3.0.htmdqx2x86DQX2X86x86"x86"
:BHXREF.HDR327cbe_programmers_guide_v3.0.htmdqx2xdrDQX2XDRXDR"XDR"
:BHXREF.HDR328cbe_programmers_guide_v3.0.htmdqx2xlcDQX2XLCXLC"XLC"
:BHXREF.HDR329cbe_programmers_guide_v3.0.htmdqx2yabootDQX2YABOOTyaboot"yaboot"
:BHXREF.HDR330cbe_programmers_guide_v3.0.htmwq196WQ196Index"Index"
