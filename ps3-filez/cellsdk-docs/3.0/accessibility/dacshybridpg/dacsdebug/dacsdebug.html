<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE html
  PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="en-us" xml:lang="en-us">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="copyright" content="(C) Copyright IBM Corporation 2007" />
<meta name="DC.rights.owner" content="(C) Copyright IBM Corporation 2007" />
<meta name="security" content="public" />
<meta name="Robots" content="index,follow" />
<meta http-equiv="PICS-Label" content='(PICS-1.1 "http://www.icra.org/ratingsv02.html" l gen true r (cz 1 lz 1 nz 1 oz 1 vz 1) "http://www.rsac.org/ratingsv01.html" l gen true r (n 0 s 0 v 0 l 0) "http://www.classify.org/safesurf/" l gen true r (SS~~000 1))' />
<meta name="DC.Type" content="concept" />
<meta name="DC.Title" content="DaCS debugging" />
<meta name="DC.Format" content="XHTML" />
<meta name="DC.Identifier" content="dacsdebug" />
<meta name="DC.Language" content="en-us" />
<!-- All rights reserved. Licensed Materials Property of IBM -->
<!-- US Government Users Restricted Rights -->
<!-- Use, duplication or disclosure restricted by -->
<!-- GSA ADP Schedule Contract with IBM Corp. -->
<link rel="stylesheet" type="text/css" href=".././ibmdita.css" />
<link rel="stylesheet" type="text/css" href=".././ibmdita.css" />
<title>DaCS debugging</title>
</head>
<body id="dacsdebug"><a name="dacsdebug"><!-- --></a>
<h1 class="topictitle1">DaCS debugging</h1>
<div><p>This chapter explains some of the alternatives provided within the SDK
to debug a Hybrid DaCS application. The standard methods of using <samp class="codeph">GDB</samp> or <samp class="codeph">printf</samp> can
still be used, but these have some unique considerations.  The Hybrid DaCS
daemons, which manage the Hybrid DaCS processes, provide logs and methods
of retaining runtime information, such as core dumps and the contents of the
current working directory. Hybrid DaCS also provides three different versions
of the library with different levels of error checking.  The base version
is optimized for performance and provides limited error checking and no tracing.
The trace version provides tracing support and the debug version provides
error checking (such as parameter verification on all the APIs).</p>
<div class="section"><h2 class="sectiontitle"><samp class="codeph">printf</samp> considerations</h2><div class="p">The easiest
and most well known way to debug a program is to add <samp class="codeph">printf</samp> statements
at strategic points. This method can be useful in hybrid application debug
provided the developer understands the following considerations:<ul><li><samp class="codeph">printf</samp> output may be interleaved between host and accelerator
application output, and may not be in exact time sequence order of invocation
between the two (or more) applications running;</li>
<li> DaCS buffers the <samp class="codeph">stderr</samp> and <samp class="codeph">stdout</samp> streams
of the accelerator. The buffers are generally flushed when a newline character
is introduced into the stream. Flushing the stream directly may have little
or no impact on displaying the data because of this behavior.</li>
</ul>
</div>
</div>
<div class="section"><h2 class="sectiontitle">Debugging with GDB</h2><p>Even though a comprehensive debugger
is not available, <samp class="codeph">gdb</samp> and <samp class="codeph">gdbserver</samp> may
be used. However, for debugging applications on the PPU the two debuggers
provided by the SDK, <samp class="codeph">ppu-gdb</samp> and <samp class="codeph">ppu-gdbserver</samp>,
should be used. These debuggers provide the same options and capabilities
as the normal <samp class="codeph">gdb</samp> programs but are specifically targeted
for the PPU architecture.</p>
<div class="p">To debug a hybrid PPU application you have
a number of options.<ol><li>If the process is running, attach to the process and debug. The process
id can be found by using executing <samp class="codeph">ps -ef</samp> on the command
line of the PPU:<pre>ppu-gdb &lt;program name&gt; &lt;process id&gt;</pre>
</li>
<li>If the process is failing use one of the following techniques to attach
the debugger to the process prior to the program ending:<ol type="a"><li><p>Use the facilities provided by DaCS to start a debugging session with <samp class="codeph">ppu-gdbserver</samp>.
In order to do this an environment variable needs to be set either prior to
launching the host application or within the application.</p>
<div class="p"><samp class="codeph">The
DACS_START_PARENT</samp> environment variable allows you to change the program
that is launched on the PPU.   Substitution variables can be used within the
command:<dl><dt class="dlterm">%e</dt>
<dd>the accelerator executable name, and</dd>
<dt class="dlterm">%a</dt>
<dd>arguments to be passed to the executable.</dd>
</dl>
For example:<pre>export DACS_START_PARENT="/usr/bin/ppu-gdbserver localhost:5678  %e %a"</pre>
</div>
<div class="p">Once
the application is started the accelerator application will wait for a <samp class="codeph">ppu-gdb</samp> client
to connect to it. (This assumes that the debugging is being performed on the
PPU client, and that the client source code is available on the PPU.) For
example:<pre>&gt; ppu-gdb <em>program</em>
(gdb) target remote localhost:<em>5678</em>
(gdb) &lt;debug as usual&gt;</pre>
</div>
<p>If debugging remotely, for example
from an x86 client, it will be necessary to find the proper levels of code
and library that are installed on the PPU for proper debugging. It will be
easier to start by debugging directly on the PPU. Refer to the <samp class="codeph">gdb</samp> documentation
for setting the shared library and source code paths.</p>
</li>
<li>Add a <samp class="codeph">sleep()</samp> call of long enough duration so that the
debugger can be started up and attached to the process.</li>
<li><p>Include a global variable and strategic while loop in the code to halt
the program so that <samp class="codeph">gdb</samp> can be attached, for example:</p>
<div class="p">Program:<pre>int  gdbwait = 1;

int main(int argc, char* argv[])
{
    .
    .
    while(gdbwait);
    .
    .
}</pre>
</div>
<div class="p">Command line:<pre>&gt; ppu-gdb program <em>23423</em>
(gdb) set gdbwait=<em>0</em>
(gdb) c</pre>
</div>
</li>
<li><div class="p">Include code to use <samp class="codeph">sigwait</samp> to wait for the user to
attach, setting the <samp class="codeph">ACCEL_DEBUG_START</samp> environment variable
for the host process and then passing it to the child using either <samp class="codeph">dacs_runtime_init()</samp> or <samp class="codeph">dacs_de_start()</samp> and
its <samp class="codeph">envp</samp> parameter, or set the the <samp class="codeph">DACS_START_ENV_LIST</samp> environment
variable <pre>DACS_START_ENV_LIST="ACCEL_DEBUG_START=Y"</pre>
 before
running the host process. Once the remote process has started it waits until
you attach to it using a debugger, for example <samp class="codeph">ppu-gdb -p &lt;<em>pid</em>&gt;</samp>.
If <samp class="codeph">ACCEL_DEBUG_START</samp> is not set the process executes normally.</div>
<p></p>
</li>
</ol>
</li>
</ol>
</div>
<div class="p">Example:<pre>#include &lt;signal.h&gt;
#include &lt;syscall.h&gt;
...
/*
In the case of ACCEL_DEBUG_START, actually wait until
the user *has* attached a debugger to this thread.
This is done here by doing an sigwait on the empty set,
which will return with EINTR after the debugger has attached.
*/
if ( getenv("ACCEL_DEBUG_START")) {
int my_pid = getpid();
fprintf(stdout,"\nPPU64: ACCEL_DEBUG_START ... 
                attach debugger to pid %d\n", my_pid);
fflush(stdout); sigset_t set; sigemptyset (&amp;set);
/* Use syscall to avoid glibc looping on EINTR. */
syscall (__NR_rt_sigtimedwait, &amp;set, (void *) 0, (void *) 0,
         _NSIG / 8);
}</pre>
</div>
</div>
<div class="section"><h2 class="sectiontitle">Daemon Support</h2><p>The Hybrid DaCS library has multiple
daemons monitoring the running DaCS applications.  The daemons log errors
and informational messages to specific system logs. The daemons provides the
capability of capturing core files that may be generated on catastrophic failure
and may retain the current working directory on the accelerator for later
examination. These are the main features that will be used when debugging
applications, but the daemons support other configuration options which may
be useful in debugging certain types of problems. These options are documented
in the <span class="filepath">/etc/dacsd.conf</span> file. The following sections discuss
the main features listed above.</p>
<p><strong>Logs</strong></p>
<div class="p">The Hybrid DaCS daemon
logs may contain invaluable information for debugging problems. The logs are
located by default in<ul><li><span class="filepath">/var/log/hdacsd.log</span> on the host, and</li>
<li><span class="filepath">/var/log/adacsd.log</span> on the accelerator.</li>
</ul>
These locations may be overridden in the daemon configuration file located
in <span class="filepath">/etc/dacsd.conf</span>.<span> For further details see <a href="../dacsrun0/initializedaemons.html#initializingthedacsdaemons">Initializing the DaCS daemons</a>.</span></div>
<p>The
logs require <samp class="codeph">root</samp> authority to view.</p>
<p>The daemons support
more detailed logging by setting the environment variable <samp class="codeph">DACS_HYBRID_DEBUG=Y</samp> when
launching the application. This variable will also be passed on to the accelerator
daemon as well. The <samp class="codeph">DACS_HYBRID_DEBUG</samp> environment variable
increases the log level in <samp class="codeph">hdacsd</samp> and <samp class="codeph">adacsd</samp> for
the duration of the application, and also creates a DaCSd SPI log for the
HE and AE applications in the <span class="filepath">/tmp</span> directory on the host
and the accelerator. The log file names are <span class="filepath">/tmp/dacsd_spi_&lt;pid&gt;.log</span>,
where <span class="filepath">&lt;pid&gt;</span> is the process id of the host or accelerator
application.</p>
<div class="p">Failures of a DaCS application often occur within the first
few DaCS functions called. The logs may provide detailed information as to
the reason of the failure. Some typical errors are:<ul><li><samp class="codeph">dacs_runtime_init()</samp> - failures during this call are usually
related to incompatibilities between a Hybrid DaCS application and the daemons
installed on the system.  A message in the logs will indicate this failure:<pre>SocketSrv    init: version mismatch</pre>
</li>
<li><p><samp class="codeph">dacs_reserve_children()</samp> - failures during this call
can usually be tracked back to errors in the <span class="filepath">/etc/dacs_topology.config</span> file.
The IP addresses and reservation visibility should be verified. For more information
on the configuration file refer to the installation guide shipped with the
SDK.</p>
<p>The actual number of accelerators allocated by this function may
not match the number requested; in particular "zero" available accelerators
may be returned with an empty DE list. This function does not return a failure
if no accelerators are available.  The user must check the return values of
this function before proceeding.</p>
</li>
<li><div class="p"><samp class="codeph">dacs_de_start()</samp> - failures during this call are typically
program and library path related issues.<ul><li>verify that the program name being passed is a full path name to the executable,
and that the executable exists on the target if the creation flag passed is <samp class="codeph">DACS_PROC_REMOTE_FILE</samp>,
or on the local host if <samp class="codeph">DACS_PROCESS_LOCAL_FILE</samp>.</li>
<li>verify that the shared libraries can be found correctly on the accelerator.
This may be done in several ways.<ul><li>Use <samp class="codeph">RPATH</samp> when linking the accelerator application, where
the <samp class="codeph">RPATH</samp> points to the exact location of the libraries on
the accelerator.</li>
<li>Use <samp class="codeph">LD_LIBRARY_PATH</samp>. Since a user's profile is not set
up when the accelerator application launches you must specify the <samp class="codeph">LD_LIBRARY_PATH</samp> in
the <samp class="codeph">DACS_START_ENV_LIST</samp> environment variable to correctly
find all of the libraries.</li>
<li>Use <samp class="codeph">ldconfig</samp> on the accelerator to cache the proper location
of the shared libraries.</li>
<li>Pass all of the libraries down with the accelerator application into the
same working directory using the <samp class="codeph">DACS_PROC_LOCAL_FILE_LIST</samp> creation
flag and a file list that contains the absolute path of the program and each
library needed to run.</li>
</ul>
</li>
</ul>
</div>
</li>
</ul>
</div>
<p><strong>Core files</strong></p>
<div class="p"> The <samp class="codeph">adacsd</samp> daemon has
a configuration option to specify the generation of core files. The configuration
file is found in <span class="filepath">/etc/dacsd.conf</span>. The following is an
excerpt of the relevant portion of this configuration file. <pre># Set curlimit on core dump resource limit for AE application.
# The curlimit is a soft limit and is less than the max limit,
# which is a hard limit.
# If a core dump is larger than the curlimit the dump will not occur.
# If child_rlimit_core=0, the current resource limit is NOT changed
#  for the AE child
# If child_rlimit_core=value&gt;0 the current resource limit will be
# changed to min(value, hard_limit).
# If child_rlimit_core=-1 the resource limit will be set to the hard
# limit--which could be infinite

        child_rlimit_core=0</pre>
</div>
<p>If this value is changed the
daemon must re-read the configuration file as described below.</p>
<p><strong>Saving
to the CWD</strong></p>
<div class="p"> The <samp class="codeph">adacsd</samp> daemon configuration file
also supports keeping the current working directory (CWD) after the process
has executed on the accelerator. This can be specified in the <span class="filepath">/etc/dacsd.conf</span> file.
The relevant excerpt is shown below:<pre># Normally the AE Current Working Directory and its contents
# are deleted when the AE process terminates.
# Set ae_cwd_keep=true if you want to prevent the
# AE Current Working Directory from being deleted.

    ae_cwd_keep=false</pre>
If this value is changed the daemon must re-read the configuration
file as described below.</div>
<div class="p">To find where the core file is being written,
issue the command:<pre>cat /proc/sys/kernel/core_pattern</pre>
If
the result is <samp class="codeph">core</samp> then the core file is written in the current
working directory. Since the current working directory is by default removed
on termination, core files will be lost without further changes. You are recommended
to change this by:<pre>echo "/tmp/core-%t-%e.%p" &gt; /proc/sys/kernel/core_pattern</pre>
which
will write any core dumps into the <span class="filepath">/tmp</span> directory with
a name of <samp class="codeph">core-&lt;<em>timestamp</em>&gt;-&lt;<em>executable</em>&gt;.&lt;<em>pid</em>&gt;</samp>.</div>
<p><strong>Making
daemon configuration changes take effect</strong></p>
<div class="p">On reboot the <samp class="codeph">adacsd</samp> will
re-read the configuration file and the changes will take effect. The changes
can be made effective immediately by sending a <samp class="codeph">SIGHUP</samp> signal
to the <samp class="codeph">adacsd</samp> daemon. For example, run these commands on
the CBE platform command line<pre>&gt; ps aux | grep dacsd  # find the process ID
&gt; kill -s SIGHUP &lt;<em>dacsd_process_ID</em>&gt;</pre>
</div>
<div class="p">The process
ID may be found in the <samp class="codeph">pidfile</samp> as well. See the line for <samp class="codeph">ADACSD_ARGS</samp> in <span class="filepath">dacsd.conf</span> :<pre>ADACSD_ARGS="--log /var/log/adacsd.log --pidfile /var/run/adacsd.pid"</pre>
and <samp class="codeph">cat
/var/run/adacsd.pid</samp></div>
</div>
<div class="section"><h2 class="sectiontitle">DaCS library versions</h2><div class="p">The optimized version of <samp class="codeph">libdacs_hybrid</samp> is
installed in <span class="filepath">/opt/cell/sdk/prototype/usr/lib64</span> and will
normally be used in production. Two other libraries are also available for
development purposes; each library provides a different set of functionality
to help in analyzing an application. To use a library temporarily <samp class="codeph">LD_LIBRARY_PATH</samp> can
be set in the local environment, and also on the accelerator by using the <samp class="codeph">DACS_START_ENV_LIST</samp> environment
variable. An example of this is:<pre>export LD_LIBRARY_PATH=/opt/cell/sdk/prototype/usr/lib64/dacs/debug
export DACS_START_ENV="LD_LIBRARY_PATH=${LD_LIBRARY_PATH}"</pre>
</div>
<div class="note"><span class="notetitle">Note:</span> The
other versions of the library must be installed on the accelerator, or the <samp class="codeph">dacs_de_start()</samp> call
will fail.</div>
<p><strong>Error Checking Library</strong></p>
<p>Hybrid DaCS provides
an error checking library to enable additional error checking, such as validation
of parameters on the DaCS APIs. The error checking library is found in directory <span class="filepath">/opt/cell/sdk/prototype/usr/lib64/dacs/debug</span>.</p>
<p>It is recommended that this library is used when first developing
a DaCS application. Once the application is running successfully the developer
can then use the regular runtime library.</p>
<p><strong>Trace enabled Library</strong></p>
<p>Hybrid
DaCS provides a tracing and debug library to track DaCS library calls. The
trace library is found in directory <span class="filepath">/opt/cell/sdk/prototype/usr/lib64/dacs/trace</span>.</p>
<p>Linking
with this library instead of the production or debug library will provide
additional traces that can be used to debug where a program is failing by
seeing what calls are made, their arguments, and the return value associated
with the call. Refer to the PDT users guide for additional capabilities of
this library and environment.</p>
</div>
</div>
<div></div></body>
</html>