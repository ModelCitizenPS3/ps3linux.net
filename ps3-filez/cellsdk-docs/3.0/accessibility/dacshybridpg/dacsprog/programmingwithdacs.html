<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE html
  PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="en-us" xml:lang="en-us">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="copyright" content="(C) Copyright IBM Corporation 2007" />
<meta name="DC.rights.owner" content="(C) Copyright IBM Corporation 2007" />
<meta name="security" content="public" />
<meta name="Robots" content="index,follow" />
<meta http-equiv="PICS-Label" content='(PICS-1.1 "http://www.icra.org/ratingsv02.html" l gen true r (cz 1 lz 1 nz 1 oz 1 vz 1) "http://www.rsac.org/ratingsv01.html" l gen true r (n 0 s 0 v 0 l 0) "http://www.classify.org/safesurf/" l gen true r (SS~~000 1))' />
<meta name="DC.Type" content="concept" />
<meta name="DC.Title" content="Programming with DaCS" />
<meta name="DC.Format" content="XHTML" />
<meta name="DC.Identifier" content="programmingwithdacs" />
<meta name="DC.Language" content="en-us" />
<!-- All rights reserved. Licensed Materials Property of IBM -->
<!-- US Government Users Restricted Rights -->
<!-- Use, duplication or disclosure restricted by -->
<!-- GSA ADP Schedule Contract with IBM Corp. -->
<link rel="stylesheet" type="text/css" href=".././ibmdita.css" />
<link rel="stylesheet" type="text/css" href=".././ibmdita.css" />
<title>Programming with DaCS</title>
</head>
<body id="programmingwithdacs"><a name="programmingwithdacs"><!-- --></a>
<h1 class="topictitle1">Programming with DaCS</h1>
<div><p><strong>DaCS API functions</strong></p>
<p>The DaCS library API services are provided
as functions in the C language.
The protocols and constants required are made available to the compiler by
including the DaCS header file <span class="filepath">dacs.h</span> as:</p>
<p><strong>#include &lt;dacs.h&gt;</strong></p>
<div class="p">In general the return value from these functions is an error code (see <a href="../../dacs/dacsappendix/errorcodes.html#errorcodes" title="This section describes the DaCS error codes">Error codes</a>). Data is
returned within parameters passed to the functions.<div class="note"><span class="notetitle">Note:</span> Implementations may
provide options, restrictions and error codes that are not specified here.</div>
<div class="note"><span class="notetitle">Note:</span> When
more than one error condition is present it is not guaranteed which one will
be reported.</div>
</div>
<p><strong>Process management model</strong></p>
<p>When working with the host and accelerators
there has to be a way to uniquely identify the participants that are communicating.
From an architectural perspective, each accelerator could have multiple processes
simultaneously running, so it is not enough simply to identify the accelerator.
Instead the unit of execution on the accelerator (the DaCS Process) must be
identified using its DaCS Element Id (DE id) and its Process Id (Pid). The
DE Id is retrieved when the accelerator is reserved (using <samp class="codeph">dacs_reserve_children()</samp>)
and the Pid when the process is started (using <samp class="codeph">dacs_de_start()</samp>). <span>Since the parent is not reserved, and no process
is started on it, two constants are provided to identify the parent: <samp class="codeph">DACS_DE_PARENT</samp> and <samp class="codeph">DACS_PID_PARENT</samp>.
Similarly, to identify the calling process itself, the constants <samp class="codeph">DACS_DE_SELF</samp> and <samp class="codeph">DACS_PID_SELF</samp> are
provided.</span></p>
<p><strong>Resource sharing model</strong></p>
<div class="p">The APIs supporting the locking primitives,
remote memory access and groups follow a consistent pattern of creation, sharing,
usage and destruction:<ul><li>Creation: An object is created which will be shared with other DEs, for
example with <samp class="codeph">dacs_remote_mem_create()</samp>.</li>
<li>Sharing: The object created is then shared by linked share and accept
calls. The creator shares the item (for instance with <samp class="codeph">dacs_remote_mem_share()</samp>),
and the DE it is shared with accepts it (in this example with <samp class="codeph">dacs_remote_mem_accept()</samp>).
These calls must be paired. When one is invoked it waits for the other to
occur. This is done for each DE the share is actioned with.</li>
<li>Usage: This may require closure (such as in the case of groups) or the object
may immediately be available for use. For instance remote memory can immediately
be used for put and get.</li>
<li>Destruction: The DEs that have accepted an item can release the item when
they are done with it (for example by calling <samp class="codeph">dacs_remote_mem_release()</samp>).
The release does not block, but notifies the creator that it is not longer
being used and cleans up any local storage. The creator does a destroy (in
this case <samp class="codeph">dacs_remote_mem_destroy()</samp>) which will wait for
all of the DEs it has shared with to release the item, and then destroy the
shared item. </li>
</ul>
</div>
<p><strong>API environment</strong></p>
<p>To make these services accessible to the runtime code each process must
create a DaCS environment. This is done by calling the special initialization
service <samp class="codeph">dacs_runtime_init()</samp>. When this service returns the
environment is set up so that all other DaCS function calls can be invoked.</p>
<p>When the DaCS environment is no longer required the process <span>must</span> call <samp class="codeph">dacs_runtime_exit()</samp> to
free all resources used by the environment.</p>
</div>
<div></div><div class="nested1" xml:lang="en-us" id="buildingadacsapplication"><a name="buildingadacsapplication"><!-- --></a><h2 class="topictitle2">Building a DaCS application</h2>
<div><p>Three versions of the DaCS libraries are provided with the DaCS packages:
optimized, debug and traced. The optimized libraries have minimal error checking
and are intended for production use. The debug libraries have much more error
checking than the optimized libraries and are intended to be used during application
development. The traced libraries are the optimized libraries with performance
and debug trace hooks in them. These are intended to be used to debug functional
and performance problems that might be encountered. The traced libraries use
the interfaces provided by the Performance Debug Tool (PDT) and require that
this tool be installed. See <a href="../../dacs/dacsappendix/perfdebugtrace.html#perfdebugtrace" title="The Performance Debugging Tool (PDT) provides trace data necessary to debug functional and performance problems for applications using the DaCS library.">Performance and debug trace</a> for
more information on configuring and using traced libraries<span>, and <a href="../../dacs/dacsdebug/dacsdebug.html#dacsdebug">DaCS debugging</a> for
debug libraries</span>.</p>
<div class="p">Both static and shared libraries are provided for the<span> x86_64 and</span> PPU.
 The desired library is selected by linking to the chosen library in the appropriate
path. <span>The static library is named <span class="filepath">libdacs.a</span>,
and the shared library is <span class="filepath">libdacs.so</span>.</span><span>The locations of
these libraries are the same on both the x86_64 and PPU:</span><div class="tablenoborder"><table cellpadding="4" cellspacing="0" summary="" rules="all" frame="border" border="1"><caption>Table 1. </caption><thead align="left"><tr valign="bottom"><th valign="bottom" width="32.121212121212125%" id="d0e143">Description</th>
<th valign="bottom" width="67.87878787878789%" id="d0e145">Library Path</th>
</tr>
</thead>
<tbody><tr><td valign="top" width="32.121212121212125%" headers="d0e143 ">Optimized</td>
<td valign="top" width="67.87878787878789%" headers="d0e145 "><span class="filepath">/prototype/usr/lib64</span> </td>
</tr>
<tr><td valign="top" width="32.121212121212125%" headers="d0e143 ">Debug</td>
<td valign="top" width="67.87878787878789%" headers="d0e145 "><span class="filepath">/prototype/usr/lib64/dacs/debug</span></td>
</tr>
<tr><td valign="top" width="32.121212121212125%" headers="d0e143 ">Traced</td>
<td valign="top" width="67.87878787878789%" headers="d0e145 "><span class="filepath">/prototype/usr/lib64/dacs/trace</span></td>
</tr>
</tbody>
</table>
</div>
</div>
</div>
</div>
<div class="nested1" xml:lang="en-us" id="affinityrequirement"><a name="affinityrequirement"><!-- --></a><h2 class="topictitle2"><span>Affinity
requirements for host applications</span></h2>
<div><div class="p">A hybrid DaCS application on the host (x86_64) must have processor affinity
to start. This can be done:<ul><li>on the command line,</li>
<li>in <samp class="codeph">mpirun</samp>, or</li>
<li>through the <samp class="codeph">sched_setaffinity</samp> function.</li>
</ul>
</div>
<div class="p">Here is a command line example to set affinity
of the shell to the first processor:<pre>taskset -p 0x00000001  $$</pre>
The
bit mask, starting with 0 from right to left, is an index to the processor
affinity setting. Bit 0 is on or off for CPU 0, bit 1 for CPU 1, and bit number <em>x</em> is
CPU number <em>x</em>.  <samp class="codeph">$$</samp> means the current process gets the
affinity setting.<pre>taskset -p $$</pre>
will return the mask
setting as an integer.  Using the <samp class="codeph">-c</samp> option makes the <samp class="codeph">taskset</samp> more
usable. For example,<pre>taskset -pc 7  $$</pre>
will set the processor
CPU affinity to CPU 7 for the current process. The <samp class="codeph">-pc</samp> parameter
sets by process and CPU number.<pre>taskset -pc  $$</pre>
will
return the current CPU setting for affinity for the current process.  <span>According to the man page for <samp class="codeph">taskset</samp> a
user must have <samp class="codeph">CAP_SYS_NICE</samp> permission to change CPU affinity. </span>See
the man page for <samp class="codeph">taskset</samp> for more details.</div>
<div class="p">To launch a DaCS application use a <samp class="codeph">taskset</samp> call,
for example:<pre>taskset 0x00000001  HelloDaCSApp Mike</pre>
 or
equivalently<pre>taskset -c 0 HelloDaCSApp Mike</pre>
where the
application program is <samp class="codeph">HelloDaCSApp</samp> and is passed an argument
of "<samp class="codeph">Mike</samp>".</div>
<p>On the accelerator system the <samp class="codeph">adacsd</samp> launch of an AE application
on a specific CBE includes setting the affinity to the CBE and its associated
memory node. If the launch is on the Cell Blade as <span>an
AE</span> no affinity is set. </p>
</div>
</div>
</body>
</html>