<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE html
  PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="en-us" xml:lang="en-us">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="copyright" content="(C) Copyright IBM Corporation 2007" />
<meta name="DC.rights.owner" content="(C) Copyright IBM Corporation 2007" />
<meta name="security" content="public" />
<meta name="Robots" content="index,follow" />
<meta http-equiv="PICS-Label" content='(PICS-1.1 "http://www.icra.org/ratingsv02.html" l gen true r (cz 1 lz 1 nz 1 oz 1 vz 1) "http://www.rsac.org/ratingsv01.html" l gen true r (n 0 s 0 v 0 l 0) "http://www.classify.org/safesurf/" l gen true r (SS~~000 1))' />
<meta name="DC.Type" content="concept" />
<meta name="DC.Title" content="Double buffering on ALF" />
<meta name="abstract" content="When transferring data in parallel with the computation, double buffering can reduce the time lost to data transfer by overlapping it with the computation time. The ALF runtime implementation on Cell BE architecture supports three different kinds of double buffering schemes." />
<meta name="description" content="When transferring data in parallel with the computation, double buffering can reduce the time lost to data transfer by overlapping it with the computation time. The ALF runtime implementation on Cell BE architecture supports three different kinds of double buffering schemes." />
<meta name="DC.subject" content="double buffering, buffer, double buffering" />
<meta name="keywords" content="double buffering, buffer, double buffering" />
<meta name="DC.Relation" scheme="URI" content="../alfprog0/programminginalf.html" />
<meta name="DC.Format" content="XHTML" />
<meta name="DC.Identifier" content="insidedoublebuffering" />
<meta name="DC.Language" content="en-us" />
<!-- All rights reserved. Licensed Materials Property of IBM -->
<!-- US Government Users Restricted Rights -->
<!-- Use, duplication or disclosure restricted by -->
<!-- GSA ADP Schedule Contract with IBM Corp. -->
<link rel="stylesheet" type="text/css" href="../ibmdita.css" />
<link rel="stylesheet" type="text/css" href="../ibmdita.css" />
<link rel="Start" href="../alfprog0/programminginalf.html" />
<title>Double buffering on ALF</title>
</head>
<body id="insidedoublebuffering"><a name="insidedoublebuffering"><!-- --></a>
<h1 class="topictitle1">Double buffering on ALF</h1>
<div><p>When transferring data in parallel with the computation, double
buffering can reduce the time lost to data transfer by overlapping it with
the computation time. The ALF runtime implementation on Cell BE architecture
supports three different kinds of double buffering schemes. </p>
<div class="fignone" id="insidedoublebuffering__alfdoublebuffer"><a name="insidedoublebuffering__alfdoublebuffer"><!-- --></a><span class="figcap">Figure 1. ALF double buffering</span><img src="accelerator_model.jpg" alt="" /></div>
<p>See <a href="#insidedoublebuffering__alfdoublebuffer">Figure 1</a> for an
illustration of how double buffering works inside ALF. The ALF runtime evaluates
each work block and decides which buffering scheme is most efficient. At each
decision point, if the conditions are met, that buffering scheme is used.
The ALF runtime first checks if the work block uses the overlapped I/O buffer.
If the overlapped I/O buffer is not used, the ALF runtime next checks the
conditions for the four-buffer scheme, then the conditions of the three-buffer
scheme. If the conditions for neither scheme are met, the ALF runtime does
not use double buffering. If the work block uses the overlapped I/O buffer,
the ALF runtime first checks the conditions for the overlapped I/O buffer
scheme, and if those conditions are not met, double buffering is not used.</p>
<p>These examples use the following assumptions:</p>
<ol><li>All SPUs have 256 KB of local memory.</li>
<li>16 KB of memory is used for code and runtime data including stack, the
task context buffer, and the data transfer list. This leaves 240 KB of local
storage for the work block buffers.</li>
<li>Transferring data in or out of accelerator memory takes one unit of time
and each computation takes two units of time.</li>
<li>The input buffer size of the work block is represented as <samp class="codeph">in_size</samp>,
the output buffer size as <samp class="codeph">out_size</samp>, and the overlapped I/O
buffer size as <samp class="codeph">overlap_size</samp>.</li>
<li>There are three computations to be done on three inputs, which produces
three outputs.</li>
</ol>
<div class="section"><h2 class="sectiontitle">Buffer schemes</h2><p>The conditions and decision tree
are further explained in the examples below.</p>
<ul><li><strong>Four-buffer scheme</strong>: In the four-buffer scheme, two buffers are
dedicated for input data and two buffers are dedicated for output data. This
buffer use is shown in the Four-buffer scheme section of <a href="#insidedoublebuffering__alfdoublebuffer">Figure 1</a>.<ul><li><strong>Conditions satisfied</strong>: The ALF runtime chooses the four-buffer scheme
if the work block does not use the overlapped I/O buffer and the buffer sizes
satisfy the following condition: 2*(in_size + out_size) &lt;= 240 KB.</li>
<li><strong>Conditions not satisfied</strong>: If the buffer sizes do not satisfy the
four-buffer scheme condition, the ALF runtime will check if the buffer sizes
satisfy the conditions of the three-buffer scheme.</li>
</ul>
 </li>
<li><strong>Three-buffer scheme</strong>: In the three-buffer scheme, the buffer is
divided into three equally sized buffers of the size max(in_size, out_size).
The buffers in this scheme are used for both input and output as shown in
the Three-buffer scheme section of <a href="#insidedoublebuffering__alfdoublebuffer">Figure 1</a>.
This scheme requires the output data movement of the previous result to be
finished before the input data movement of the next work block starts, so
the DMA operations must be done in order. The advantage of this approach is
that for a specific work block, if the input and output buffer are almost
the same size, the total effective buffer size can be 2*240/3 = 160 KB.<ul><li><strong>Conditions satisfied</strong>: The ALF runtime chooses the three-buffer
scheme if the work block does not use the overlapped I/O buffer and the buffer
sizes satisfy the following condition: 3*max(in_size, out_size) &lt;= 240
KB.</li>
<li><strong>Conditions not satisfied</strong>: If the conditions are not satisfied,
the single-buffer scheme is used.</li>
</ul>
</li>
<li><strong>Overlapped I/O buffer scheme</strong>: In the overlapped I/O buffer scheme,
two contiguous buffers are allocated as shown in the Overlapped I/O buffer
scheme section of <a href="#insidedoublebuffering__alfdoublebuffer">Figure 1</a>.
The overlapped I/O buffer scheme requires the output data movement of the
previous result to be finished before the input data movement of the next
work block starts.<ul><li><strong>Conditions satisfied</strong>: The ALF runtime chooses the overlapped I/O
buffer scheme if the work block uses the overlapped I/O buffer and the buffer
sizes satisfy the following condition: 2*(in_size + overlap_size + out_size) &lt;=
240 KB.</li>
<li><strong>Conditions not satisfied</strong>: If the conditions are not satisfied,
the single-buffer scheme is used.</li>
</ul>
</li>
<li><strong>Single-buffer scheme</strong>: If none of the cases outlined above can be
satisfied, double buffering is not used, but performance might not be optimal.</li>
</ul>
<p>When creating buffers and data partitions, remember the conditions
of these buffering schemes. If your buffer sizes can meet the conditions required
for double buffering, it can result in a performance gain, but double buffering
does not double the performances in all cases. When the time periods required
by data movements and computation are significantly different, the problem
becomes either I/O-bound or computing-bound. In this case, enlarging the buffers
to allow more data for a single computation might improve the performance
even with a single buffer.</p>
</div>
</div>
<div>
<div class="familylinks">
<div class="parentlink"><strong>Parent topic:</strong> <a href="../alfprog0/programminginalf.html" title="">Programming with ALF</a></div>
</div>
</div></body>
</html>