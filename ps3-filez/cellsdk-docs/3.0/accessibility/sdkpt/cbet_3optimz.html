<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE html
  PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="en-us" xml:lang="en-us">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="copyright" content="(C) Copyright IBM Corporation 2007" />
<meta name="DC.rights.owner" content="(C) Copyright IBM Corporation 2007" />
<meta name="security" content="public" />
<meta name="Robots" content="index,follow" />
<meta http-equiv="PICS-Label" content='(PICS-1.1 "http://www.icra.org/ratingsv02.html" l gen true r (cz 1 lz 1 nz 1 oz 1 vz 1) "http://www.rsac.org/ratingsv01.html" l gen true r (n 0 s 0 v 0 l 0) "http://www.classify.org/safesurf/" l gen true r (SS~~000 1))' />
<meta name="DC.Type" content="concept" />
<meta name="DC.Title" content="Optimizations" />
<meta name="abstract" content="To eliminate stalls and improve the CPI — and ultimately the performance — the compiler needs more instructions to schedule, so that the program does not stall. The SPE's large register file allows the compiler or the programmer to unroll loops." />
<meta name="description" content="To eliminate stalls and improve the CPI — and ultimately the performance — the compiler needs more instructions to schedule, so that the program does not stall. The SPE's large register file allows the compiler or the programmer to unroll loops." />
<meta name="DC.subject" content="inter-loop dependencies, dependencies, loop-carried dependencies, loop unrolling, scalar loads, DMA transfers, multibuffering" />
<meta name="keywords" content="inter-loop dependencies, dependencies, loop-carried dependencies, loop unrolling, scalar loads, DMA transfers, multibuffering" />
<meta name="DC.Relation" scheme="URI" content="cbet_3tune.html" />
<meta name="DC.Relation" scheme="URI" content="cbet_3dynthr.html" />
<meta name="DC.Relation" scheme="URI" content="cbet_3staticanaly.html" />
<meta name="DC.Format" content="XHTML" />
<meta name="DC.Identifier" content="doptimz" />
<meta name="DC.Language" content="en-us" />
<!-- All rights reserved. Licensed Materials Property of IBM -->
<!-- US Government Users Restricted Rights -->
<!-- Use, duplication or disclosure restricted by -->
<!-- GSA ADP Schedule Contract with IBM Corp. -->
<link rel="stylesheet" type="text/css" href="./ibmdita.css" />
<link rel="Start" href="cbet_3tune.html" />
<link rel="Prev" href="cbet_3dynthr.html" />
<link rel="Next" href="cbet_3staticanaly.html" />
<title>Optimizations</title>
</head>
<body id="doptimz"><a name="doptimz"><!-- --></a>
<h1 class="topictitle1">Optimizations</h1>
<div><p>To eliminate stalls and improve the CPI — and ultimately the performance
— the compiler needs more instructions to schedule, so that the program does
not stall. The SPE's large register file allows the compiler or the programmer
to unroll loops.</p>
<p>In our example program, there are no  inter-loop dependencies (loop-carried
dependencies), and our dynamic analysis shows that the register usage is fairly
small, so moderately aggressive unrolling will not produce register spilling
(that is, registers having to be written into temporary stack storage). </p>
<p>Most compilers can automatically unroll loops. Sometimes this is effective.
But because automatic loop unrolling is not always effective, or because the
programmer wants explicit control to manage the limited local store, this
example shows how to manually unroll the loop. </p>
<div class="p">The first pass of optimizations include:<ul><li>Unroll the  loop to provide additional instructions for interleaving.</li>
<li>Load DMA-buffer contents into local nonvolatile registers to eliminate
volatile migration constraints.</li>
<li>Eliminate  scalar loads (the <samp class="codeph">inv_mass</samp> variable).</li>
<li>Eliminate extra multiplies of  <samp class="codeph">dt*inv_mass</samp> and <samp class="codeph">splat</samp> the
products after the SIMD multiply, instead of before the multiply.</li>
<li>Interleave  DMA transfers with computation by multibuffering the inputs
and outputs to eliminate (or reduce) DMA stalls. These stalls are not reflected
in the static and dynamic analyses. In the process of adding double buffering,
the inner loop is moved into a function, so that the code need not be repeated.</li>
</ul>
</div>
<div class="p">The following SPE code results from these optimizations. Among the changes
are the addition of a <samp class="codeph">GET</samp> instruction with a barrier suffix
(<samp class="codeph">B</samp>), accomplished by the <samp class="codeph">spu_mfcdma32()</samp> intrinsic
with the <samp class="codeph">MFC_GETB_CMD</samp> parameter. This <samp class="codeph">GET</samp> is
the barrier form of <samp class="codeph">MFC_GET_CMD</samp>. The barrier form is used
to ensure that previously computed results are <samp class="codeph">put</samp> before
the <samp class="codeph">get</samp> for the next buffer's data.<pre>#include &lt;spu_intrinsics.h&gt;
#include &lt;spu_mfcio.h&gt;
#include "particle.h"

#define PARTICLES_PER_BLOCK             1024

// Local store structures and buffers.
volatile context ctx;
volatile vector float pos[2][PARTICLES_PER_BLOCK];
volatile vector float vel[2][PARTICLES_PER_BLOCK];
volatile vector float inv_mass[2][PARTICLES_PER_BLOCK/4];

void process_buffer(int buffer, int cnt, vector float dt_v)
{
  int i;
  volatile vector float *p_inv_mass_v;
  vector float force_v, inv_mass_v;
  vector float pos0, pos1, pos2, pos3;
  vector float vel0, vel1, vel2, vel3;
  vector float dt_inv_mass_v, dt_inv_mass_v_0, dt_inv_mass_v_1, 
    dt_inv_mass_v_2, dt_inv_mass_v_3;
  vector unsigned char splat_word_0 = 
	(vector unsigned char){0, 1, 2, 3, 0, 1, 2, 3, 0, 1, 2, 3, 0, 1, 2, 3};
  vector unsigned char splat_word_1 = 
	(vector unsigned char){4, 5, 6, 7, 4, 5, 6, 7, 4, 5, 6, 7, 4, 5, 6, 7};
  vector unsigned char splat_word_2 = 
	(vector unsigned char){8, 9,10,11, 8, 9,10,11, 8, 9,10,11, 8, 9,10,11};
  vector unsigned char splat_word_3 = 
	(vector unsigned char){12,13,14,15,12,13,14,15,12,13,14,15,12,13,14,15};

  p_inv_mass_v = (volatile vector float *)&amp;inv_mass[buffer][0]; 
  force_v = ctx.force_v;

  // Compute the step in time for the block of particles, four 
  // particle at a time.
  for (i=0; i&lt;cnt; i+=4) {
    inv_mass_v = *p_inv_mass_v++;
    
    pos0 = pos[buffer][i+0];
    pos1 = pos[buffer][i+1];
    pos2 = pos[buffer][i+2];
    pos3 = pos[buffer][i+3];

    vel0 = vel[buffer][i+0];
    vel1 = vel[buffer][i+1];
    vel2 = vel[buffer][i+2];
    vel3 = vel[buffer][i+3];

    dt_inv_mass_v = spu_mul(dt_v, inv_mass_v);

    pos0 = spu_madd(vel0, dt_v, pos0);
    pos1 = spu_madd(vel1, dt_v, pos1);
    pos2 = spu_madd(vel2, dt_v, pos2);
    pos3 = spu_madd(vel3, dt_v, pos3);

    dt_inv_mass_v_0 = spu_shuffle(dt_inv_mass_v, dt_inv_mass_v, splat_word_0);
    dt_inv_mass_v_1 = spu_shuffle(dt_inv_mass_v, dt_inv_mass_v, splat_word_1);
    dt_inv_mass_v_2 = spu_shuffle(dt_inv_mass_v, dt_inv_mass_v, splat_word_2);
    dt_inv_mass_v_3 = spu_shuffle(dt_inv_mass_v, dt_inv_mass_v, splat_word_3);

    vel0 = spu_madd(dt_inv_mass_v_0, force_v, vel0);
    vel1 = spu_madd(dt_inv_mass_v_1, force_v, vel1);
    vel2 = spu_madd(dt_inv_mass_v_2, force_v, vel2);
    vel3 = spu_madd(dt_inv_mass_v_3, force_v, vel3);

    pos[buffer][i+0] = pos0;
    pos[buffer][i+1] = pos1;
    pos[buffer][i+2] = pos2;
    pos[buffer][i+3] = pos3;

    vel[buffer][i+0] = vel0;
    vel[buffer][i+1] = vel1;
    vel[buffer][i+2] = vel2;
    vel[buffer][i+3] = vel3;
  }
}


int main(unsigned long long spe_id, unsigned long long argv)
{
  int buffer, next_buffer;
  int cnt, next_cnt, left;
  float time, dt;
  vector float dt_v;
  volatile vector float *ctx_pos_v, *ctx_vel_v;
  volatile vector float *next_ctx_pos_v, *next_ctx_vel_v;
  volatile float *ctx_inv_mass, *next_ctx_inv_mass;
  unsigned int tags[2];

  // Reserve a pair of DMA tag IDs
  tags[0] = mfc_tag_reserve();
  tags[1] = mfc_tag_reserve();

  // Input parameter argv is a pointer to the particle context.
  // Fetch the context, waiting for it to complete.
  spu_writech(MFC_WrTagMask, 1 &lt;&lt; tags[0]);
  spu_mfcdma32((void *)(&amp;ctx), (unsigned int)argv, sizeof(context), tags[0],
    MFC_GET_CMD);
  (void)spu_mfcstat(MFC_TAG_UPDATE_ALL);

  dt = ctx.dt;
  dt_v = spu_splats(dt);

  // For each step in time
  for (time=0; time&lt;END_OF_TIME; time += dt) {
    // For each double buffered block of particles
    left = ctx.particles;

    cnt = (left &lt; PARTICLES_PER_BLOCK) ? left : PARTICLES_PER_BLOCK;

    ctx_pos_v = ctx.pos_v;
    ctx_vel_v = ctx.vel_v;
    ctx_inv_mass = ctx.inv_mass;

    // Prefetch first buffer of input data
    buffer = 0;
    spu_mfcdma32((void *)(pos), (unsigned int)(ctx_pos_v), cnt * 
      sizeof(vector float), tags[0], MFC_GETB_CMD);
    spu_mfcdma32((void *)(vel), (unsigned int)(ctx_vel_v), cnt * 
      sizeof(vector float), tags[0], MFC_GET_CMD);
    spu_mfcdma32((void *)(inv_mass), (unsigned int)(ctx_inv_mass), cnt * 
      sizeof(float), tags[0], MFC_GET_CMD);

    while (cnt &lt; left) {
      left -= cnt;

      next_ctx_pos_v = ctx_pos_v + cnt;
      next_ctx_vel_v = ctx_vel_v + cnt;
      next_ctx_inv_mass = ctx_inv_mass + cnt;
      next_cnt = (left &lt; PARTICLES_PER_BLOCK) ? left : PARTICLES_PER_BLOCK;

      // Prefetch next buffer so the data is available for computation on next 
      //   loop iteration.
      // The first DMA is barriered so that we don't GET data before the 
      //   previous iteration's data is PUT.
      next_buffer = buffer^1;

      spu_mfcdma32((void *)(&amp;pos[next_buffer][0]), (unsigned int)(next_ctx_pos_v), 
        next_cnt * sizeof(vector float), tags[next_buffer], MFC_GETB_CMD);
      spu_mfcdma32((void *)(&amp;vel[next_buffer][0]), (unsigned int)(next_ctx_vel_v), 
        next_cnt * sizeof(vector float), tags[next_buffer], MFC_GET_CMD);
      spu_mfcdma32((void *)(&amp;inv_mass[next_buffer][0]), (unsigned int)
        (next_ctx_inv_mass), next_cnt * sizeof(float), tags[next_buffer],
         MFC_GET_CMD);
      
      // Wait for previously prefetched data
      spu_writech(MFC_WrTagMask, 1 &lt;&lt; tags[buffer]);
      (void)spu_mfcstat(MFC_TAG_UPDATE_ALL);

      process_buffer(buffer, cnt, dt_v);

      // Put the buffer's position and velocity data back into main storage
      spu_mfcdma32((void *)(&amp;pos[buffer][0]), (unsigned int)(ctx_pos_v), cnt * 
        sizeof(vector float), tags[buffer], MFC_PUT_CMD);
      spu_mfcdma32((void *)(&amp;vel[buffer][0]), (unsigned int)(ctx_vel_v), cnt * 
        sizeof(vector float), tags[buffer], MFC_PUT_CMD);
      
      ctx_pos_v = next_ctx_pos_v;
      ctx_vel_v = next_ctx_vel_v;
      ctx_inv_mass = next_ctx_inv_mass;

      buffer = next_buffer;
      cnt = next_cnt;             
    }

    // Wait for previously prefetched data
    spu_writech(MFC_WrTagMask, 1 &lt;&lt; tags[buffer]);
    (void)spu_mfcstat(MFC_TAG_UPDATE_ALL);

    process_buffer(buffer, cnt, dt_v);

    // Put the buffer's position and velocity data back into main storage
    spu_mfcdma32((void *)(&amp;pos[buffer][0]), (unsigned int)(ctx_pos_v), cnt * 
      sizeof(vector float), tags[buffer], MFC_PUT_CMD);
    spu_mfcdma32((void *)(&amp;vel[buffer][0]), (unsigned int)(ctx_vel_v), cnt * 
      sizeof(vector float), tags[buffer], MFC_PUT_CMD);

    // Wait for DMAs to complete before starting the next step in time.
    spu_writech(MFC_WrTagMask, 1 &lt;&lt; tags[buffer]);
    (void)spu_mfcstat(MFC_TAG_UPDATE_ALL);  
  }

  return (0);
}</pre>
</div>
</div>
<div>
<div class="familylinks">
<div class="parentlink"><strong>Parent topic:</strong> <a href="cbet_3tune.html">Example 1: Tuning SPE performance with static and dynamic timing analysis</a></div>
<div class="previouslink"><strong>Previous topic:</strong> <a href="cbet_3dynthr.html" title="The listing below shows a dynamic timing analysis on the same SPE inner loop using the IBM Full System Simulator for the Cell Broadband Engine.">Dynamic analysis of SPE threads</a></div>
<div class="nextlink"><strong>Next topic:</strong> <a href="cbet_3staticanaly.html" title="The listing below shows a spu_timing static timing analysis for the optimized SPE thread (process _buffer subroutine only).">Static analysis of optimization</a></div>
</div>
</div></body>
</html>