<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE html
  PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="en-us" xml:lang="en-us">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="copyright" content="(C) Copyright IBM Corporation 2007" />
<meta name="DC.rights.owner" content="(C) Copyright IBM Corporation 2007" />
<meta name="security" content="public" />
<meta name="Robots" content="index,follow" />
<meta http-equiv="PICS-Label" content='(PICS-1.1 "http://www.icra.org/ratingsv02.html" l gen true r (cz 1 lz 1 nz 1 oz 1 vz 1) "http://www.rsac.org/ratingsv01.html" l gen true r (n 0 s 0 v 0 l 0) "http://www.classify.org/safesurf/" l gen true r (SS~~000 1))' />
<meta name="DC.Type" content="concept" />
<meta name="DC.Title" content="Moving double-buffered data" />
<meta name="abstract" content="SPE programs use DMA transfers to move data and instructions between main storage and the local store (LS) in the SPE." />
<meta name="description" content="SPE programs use DMA transfers to move data and instructions between main storage and the local store (LS) in the SPE." />
<meta name="DC.subject" content="double buffering (programming example), double buffering, multibuffering, fenced command option, barrier commands" />
<meta name="keywords" content="double buffering (programming example), double buffering, multibuffering, fenced command option, barrier commands" />
<meta name="DC.Relation" scheme="URI" content="cbet_3codemeth.html" />
<meta name="DC.Relation" scheme="URI" content="cbet_3dmaltran.html" />
<meta name="DC.Relation" scheme="URI" content="cbet_3vectloop.html" />
<meta name="DC.Format" content="XHTML" />
<meta name="DC.Identifier" content="dmovex2dat" />
<meta name="DC.Language" content="en-us" />
<!-- All rights reserved. Licensed Materials Property of IBM -->
<!-- US Government Users Restricted Rights -->
<!-- Use, duplication or disclosure restricted by -->
<!-- GSA ADP Schedule Contract with IBM Corp. -->
<link rel="stylesheet" type="text/css" href="./ibmdita.css" />
<link rel="Start" href="cbet_3codemeth.html" />
<link rel="Prev" href="cbet_3dmaltran.html" />
<link rel="Next" href="cbet_3vectloop.html" />
<title>Moving double-buffered data</title>
</head>
<body id="dmovex2dat"><a name="dmovex2dat"><!-- --></a>
<h1 class="topictitle1">Moving double-buffered data</h1>
<div><p>SPE programs use <em>DMA transfers</em> to move data and instructions
between main storage and the local store (LS) in the SPE.</p>
<div class="p">Consider an SPE program that requires large amounts of data from main storage.
The following is a simple scheme to achieve that data transfer:<ol><li>Start a DMA data transfer from main storage to buffer B in the LS.</li>
<li>Wait for the transfer to complete.</li>
<li>Use the data in buffer <samp class="codeph">B</samp>.</li>
<li>Repeat.</li>
</ol>
</div>
<p>This method wastes a great deal of time waiting for DMA transfers to complete.
We can speed up the process significantly by allocating two buffers, <samp class="codeph">B<sub>0</sub></samp> and <samp class="codeph">B<sub>1</sub></samp> ,
and overlapping computation on one buffer with data transfer in the other.
This technique is called <em>double buffering</em>. <a href="#dmovex2dat__fstructarrays">Figure 1</a> shows
a flow diagram for this double buffering scheme. </p>
<p>Double buffering is a form of  <em>multibuffering</em>,  which is the method
of using multiple buffers in a circular queue to overlap processing and data
transfer. </p>
<div class="p"><div class="figtopbot" id="dmovex2dat__fstructarrays"><a name="dmovex2dat__fstructarrays"><!-- --></a><span class="figcap">Figure 1. DMA
transfers using a double-buffering method</span><img src="cbet_ch3_g008.jpg" alt="DMA transfers using a double-buffering method" /></div>
</div>
<div class="p">The following C-language example illustrates double buffering:<pre>/* Example C code demonstrating double buffering using 
 * buffers B[0] and B[1]. In this example, an array of data 
 * starting at the effective address eahi|ealow is DMAed 
 * into the SPU's local store in 4-KB chunks and processed 
 * by the use_data subroutine.
 */
#include &lt;spu_intrinsics.h&gt;
#include "spu_mfcio.h"

#define BUFFER_SIZE	 4096

volatile unsigned char B[2][BUFFER_SIZE] __attribute__ ((aligned(128)));

void double_buffer_example(unsigned int eahi, unsigned int ealow, int buffers)
{
  int next_idx, buf_idx = 0;

  // Initiate DMA transfer
  spu_mfcdma64(B[buf_idx], eahi, ealow, BUFFER_SIZE, buf_idx, MFC_GET_CMD);
  ealow += BUFFER_SIZE;

  while (--buffers) {
    next_idx = buf_idx ^ 1;

    // Initiate next DMA transfer
    spu_mfcdma64(B[next_idx], eahi, ealow, BUFFER_SIZE, next_idx, MFC_GET_CMD);
    ealow += BUFFER_SIZE;

    // Wait for previous transfer to complete
    spu_writech(MFC_WrTagMask, 1 &lt;&lt; buf_idx);
    (void)spu_mfcstat(MFC_TAG_UPDATE_ALL);

    // Use the data from the previous transfer
    use_data(B[buf_idx]);

    buf_idx = next_idx;
  }

  // Wait for last transfer to complete
  spu_writech(MFC_WrTagMask, 1 &lt;&lt; buf_idx);
  (void)spu_mfcstat(MFC_TAG_UPDATE_ALL);

  // Use the data from the last transfer
  use_data(B[buf_idx]);
}</pre>
<div class="note"><span class="notetitle">Note:</span> The above example is hardcoded
to use tag ids <samp class="codeph">0</samp> and <samp class="codeph">1</samp>. Applications are
encouraged to use the tag manager functions to reserve tag ids for cooperative
allocations of tags between independent software components.</div>
</div>
<p>To use double buffering effectively, follow these rules for DMA transfers
on the SPE:</p>
<ul><li>Use multiple LS buffers.</li>
<li>Use unique DMA tag IDs, one for each LS buffer or logical group of LS
buffers.</li>
<li>Use <samp class="codeph">fenced</samp> command options to order the DMA transfers
within a tag group.</li>
<li>Use  <samp class="codeph">barrier</samp> command options to order DMA transfers within
the MFC's DMA controller.</li>
</ul>
<p>The purpose of double buffering is to maximize the time spent in the compute
phase of a program and minimize the time spent waiting for DMA transfers to
complete. Let τ<sub>t</sub> represent the time required to transfer a buffer
B, and let τ<sub>c</sub> represent the time required to compute on data contained
in that buffer. In general, the higher the ratio τ<sub>t</sub>/τ<sub>c</sub>,
the more performance benefit an application will realize from a double-buffering
scheme.</p>
</div>
<div>
<div class="familylinks">
<div class="parentlink"><strong>Parent topic:</strong> <a href="cbet_3codemeth.html" title="The sections included here describe some coding methods, with examples in SPU assembly language, C language, SPU C-language intrinsics, and MFC commands, or in a combination thereof.">Coding methods and examples</a></div>
<div class="previouslink"><strong>Previous topic:</strong> <a href="cbet_3dmaltran.html" title="A DMA list is a sequence of transfer elements (or list elements) that, together with an initiating DMA-list command, specifies a sequence of DMA transfers between a single area of LS and possibly discontinuous areas in main storage.">DMA-list transfers</a></div>
<div class="nextlink"><strong>Next topic:</strong> <a href="cbet_3vectloop.html" title="A compiler that automatically merges scalar data into a parallel-packed SIMD data structure is called an auto-vectorizing compiler. Such compilers must handle all the high-level language constructs, and therefore do not always produce optimal code.">Vectorizing a loop</a></div>
</div>
</div></body>
</html>