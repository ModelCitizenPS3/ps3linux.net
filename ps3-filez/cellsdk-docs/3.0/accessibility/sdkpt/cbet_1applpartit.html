<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE html
  PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="en-us" xml:lang="en-us">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="copyright" content="(C) Copyright IBM Corporation 2007" />
<meta name="DC.rights.owner" content="(C) Copyright IBM Corporation 2007" />
<meta name="security" content="public" />
<meta name="Robots" content="index,follow" />
<meta http-equiv="PICS-Label" content='(PICS-1.1 "http://www.icra.org/ratingsv02.html" l gen true r (cz 1 lz 1 nz 1 oz 1 vz 1) "http://www.rsac.org/ratingsv01.html" l gen true r (n 0 s 0 v 0 l 0) "http://www.classify.org/safesurf/" l gen true r (SS~~000 1))' />
<meta name="DC.Type" content="concept" />
<meta name="DC.Title" content="Application partitioning" />
<meta name="abstract" content="Programs running on the Cell Broadband Engine’s nine processor elements typically partition the work among the available processor elements." />
<meta name="description" content="Programs running on the Cell Broadband Engine’s nine processor elements typically partition the work among the available processor elements." />
<meta name="DC.subject" content="partitioning, PPE-centric models, SPE-centric model, multi-stage pipeline model, parallel-stages model, service model" />
<meta name="keywords" content="partitioning, PPE-centric models, SPE-centric model, multi-stage pipeline model, parallel-stages model, service model" />
<meta name="DC.Relation" scheme="URI" content="cbet_1prog_ovw.html" />
<meta name="DC.Relation" scheme="URI" content="cbet_1runenv.html" />
<meta name="DC.Format" content="XHTML" />
<meta name="DC.Identifier" content="aplpartit" />
<meta name="DC.Language" content="en-us" />
<!-- All rights reserved. Licensed Materials Property of IBM -->
<!-- US Government Users Restricted Rights -->
<!-- Use, duplication or disclosure restricted by -->
<!-- GSA ADP Schedule Contract with IBM Corp. -->
<link rel="stylesheet" type="text/css" href="./ibmdita.css" />
<link rel="Start" href="cbet_1prog_ovw.html" />
<link rel="Prev" href="cbet_1runenv.html" />
<title>Application partitioning</title>
</head>
<body id="aplpartit"><a name="aplpartit"><!-- --></a>
<h1 class="topictitle1">Application partitioning</h1>
<div><p>Programs running on the <span>Cell Broadband Engine</span>’s
nine processor elements typically partition the work among the available processor
elements.</p>
<div class="p">In determining when and how to distribute the workload and data, take into
account the following considerations: <ul><li>processing-load distribution,</li>
<li>program structure,</li>
<li>program data flow and data access patterns,</li>
<li>cost, in time and complexity of code movement and data movement among
processors, and</li>
<li>cost of loading the bus and bus attachments.</li>
</ul>
</div>
<div class="p">The main model for partitioning an application is <em>PPE-centric</em>, as
shown in <a href="#aplpartit__fapprmodel">Figure 1</a>.<div class="figtopbot" id="aplpartit__fapprmodel"><a name="aplpartit__fapprmodel"><!-- --></a><span class="figcap">Figure 1. Application partitioning
model</span><img src="cbet_ch1_g007.jpg" alt="application partitioning model" /></div>
</div>
<p>In the <em>PPE-centric model</em>, the main application runs on the PPE,
and individual tasks are off-loaded to the SPEs. The PPE then waits for, and
coordinates, the results returning from the SPEs. This model fits an application
with serial data and parallel computation.</p>
<p>In the <em>SPE-centric model</em>, most of the application code is distributed
among the SPEs. The PPE acts as a centralized resource manager for the SPEs.
Each SPE fetches its next work item from main storage (or its own local store)
when it completes its current work.</p>
<div class="p">There are <em>three</em> ways in which the SPEs can be used in the PPE-centric
model:<ul><li>the multistage pipeline model,</li>
<li>the parallel stages model, and</li>
<li>the services model. </li>
</ul>
The first two of these are shown in <a href="#aplpartit__fppestage">Figure 2</a>.
 </div>
<p>If a task requires sequential stages, the SPEs can act as a <em>multistage
pipeline</em>. The left side of <a href="#aplpartit__fppestage">Figure 2</a> shows
a multistage pipeline. Here, the stream of data is sent into the first SPE,
which performs the first stage of the processing. The first SPE then passes
the data to the next SPE for the next stage of processing. After the last
SPE has done the final stage of processing on its data, that data is returned
to the PPE. As with any pipeline architecture, parallel processing occurs,
with various portions of data in different stages of being processed.</p>
<p>Multistage pipelining is typically avoided because of the difficulty of
load balancing. In addition, the multistage model increases the data-movement
requirement because data must be moved for each stage of the pipeline.</p>
<div class="p"><div class="figtopbot" id="aplpartit__fppestage"><a name="aplpartit__fppestage"><!-- --></a><span class="figcap">Figure 2. PPE-centric
multistage pipeline model and parallel stages model</span><img src="cbet_ch1_g008.jpg" alt="PPE-centric multistage pipeline and parallel stages models" /></div>
</div>
<p>If the task to be performed is not a multistage task, but a task in which
there is a large amount of data that can be partitioned and acted on at the
same time, then it typically make sense to use SPEs to process different portions
of that data in parallel. This <em>parallel stages model</em> is shown on the
right side of <a href="#aplpartit__fppestage">Figure 2</a>.</p>
<p>The third way in which SPEs can be used in a PPE-centric model is the <em>services
model</em>. In the services model, the PPE assigns different services to different
SPEs, and the PPE’s main process calls upon the appropriate SPE when a particular
service is needed.</p>
<div class="p"><a href="#aplpartit__fppeservice">Figure 3</a> shows the <em>PPE-centric services
model</em>. Here, one SPE processes data encryption, another SPE processes
MPEG encoding, and a third SPE processes curve analysis. Fixed static allocation
of SPU services should be avoided. These services should be virtualized and
managed on a demand-initiated basis.<div class="figtopbot" id="aplpartit__fppeservice"><a name="aplpartit__fppeservice"><!-- --></a><span class="figcap">Figure 3. PPE-centric services model</span><img src="cbet_ch1_g009.jpg" alt="PPE-centric services model" /></div>
</div>
<p>For a more detailed view of programming models, see <a href="cbet_4progmodel.html#dprogmodel" title="On any processor, coding optimizations are achieved by exploiting the unique features of the hardware. In the case of the Cell Broadband Engine, the large number of SPEs, their large register file, and their ability to hide main-storage latency with concurrent computation and DMA transfers support many interesting programming models.">Programming models</a>.</p>
</div>
<div>
<div class="familylinks">
<div class="parentlink"><strong>Parent topic:</strong> <a href="cbet_1prog_ovw.html" title="The instruction set for the PPE is an extended version of the PowerPC instruction set. The extensions consist of the Vector/SIMD Multimedia Extension instruction set plus a few additions and changes to PowerPC instructions.">Programming Overview</a></div>
<div class="previouslink"><strong>Previous topic:</strong> <a href="cbet_1runenv.html" title="The PPE runs PowerPC applications and operating systems, which may include Vector/SIMD Multimedia Extension instructions.">The runtime environment</a></div>
</div>
</div></body>
</html>