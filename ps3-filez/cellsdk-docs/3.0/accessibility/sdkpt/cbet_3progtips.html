<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE html
  PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="en-us" xml:lang="en-us">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="copyright" content="(C) Copyright IBM Corporation 2007" />
<meta name="DC.rights.owner" content="(C) Copyright IBM Corporation 2007" />
<meta name="security" content="public" />
<meta name="Robots" content="index,follow" />
<meta http-equiv="PICS-Label" content='(PICS-1.1 "http://www.icra.org/ratingsv02.html" l gen true r (cz 1 lz 1 nz 1 oz 1 vz 1) "http://www.rsac.org/ratingsv01.html" l gen true r (n 0 s 0 v 0 l 0) "http://www.classify.org/safesurf/" l gen true r (SS~~000 1))' />
<meta name="DC.Type" content="concept" />
<meta name="DC.Title" content="General SPE programming tips" />
<meta name="abstract" content="This section contains a short summary of general tips for optimizing the performance of SPE programs." />
<meta name="description" content="This section contains a short summary of general tips for optimizing the performance of SPE programs." />
<meta name="DC.subject" content="programming tips" />
<meta name="keywords" content="programming tips" />
<meta name="DC.Relation" scheme="URI" content="cbet_3progspes.html" />
<meta name="DC.Relation" scheme="URI" content="cbet_3perfanlz.html" />
<meta name="DC.Format" content="XHTML" />
<meta name="DC.Identifier" content="dprogtips" />
<meta name="DC.Language" content="en-us" />
<!-- All rights reserved. Licensed Materials Property of IBM -->
<!-- US Government Users Restricted Rights -->
<!-- Use, duplication or disclosure restricted by -->
<!-- GSA ADP Schedule Contract with IBM Corp. -->
<link rel="stylesheet" type="text/css" href="./ibmdita.css" />
<link rel="Start" href="cbet_3progspes.html" />
<link rel="Prev" href="cbet_3perfanlz.html" />
<title>General SPE programming tips</title>
</head>
<body id="dprogtips"><a name="dprogtips"><!-- --></a>
<h1 class="topictitle1">General SPE programming tips</h1>
<div><p>This section contains a short summary of general tips for optimizing
the performance of SPE programs.</p>
<ul><li><em>Local Store</em><ul><li>Design for the LS size. The LS holds up to 256 KB for the program, stack,
local data structures, and DMA buffers. One can do a lot with 256 KB, but
be aware of this size. </li>
<li>Use overlays (runtime download program kernels) to build complex function
servers in the LS (see <a href="cbet_4speoverl.html#dspeoverl" title="When code does not fit in an SPE's local store, overlays can be useful.">SPE overlays</a>).</li>
</ul>
</li>
<li><em>DMA Transfers</em><ul><li>Use SPE-initiated DMA transfers rather than PPE-initiated DMA transfers.
There are more SPEs than the one PPE, and the PPE can enqueue only eight DMA
requests whereas each SPE can enqueue 16. </li>
<li>Overlap DMA with computation by double buffering or multibuffering (see <a href="cbet_3movex2dat.html#dmovex2dat" title="SPE programs use DMA transfers to move data and instructions between main storage and the local store (LS) in the SPE.">Moving double-buffered data</a>). Multibuffer code or (typically)
data. </li>
<li>Use double buffering to hide memory latency.</li>
<li>Use  <samp class="codeph">fence</samp> command options to order DMA transfers within
a tag group.</li>
<li>Use  <samp class="codeph">barrier</samp> command options to order DMA transfers within
the queue. </li>
</ul>
</li>
<li><em>Loops</em><ul><li>Unroll loops to reduce dependencies and increase dual-issue rates. This
exploits the large SPU register file.</li>
<li>Compiler auto-unrolling is not perfect, but pretty good.</li>
</ul>
</li>
<li><em>SIMD Strategy</em><ul><li>Choose an SIMD strategy appropriate for your algorithm. For example:</li>
<li>Evaluate array-of-structure (AOS) organization. For graphics vertices,
this organization (also called or vector-across) can have more-efficient code
size and simpler DMA needs, but less-efficient computation unless the code
is unrolled.</li>
<li>Evaluate structure-of-arrays (SOA) organization. For graphics vertices,
this organization (also called parallel-array) can be easier to <samp class="codeph">SIMDize</samp>,
but the data must be maintained in separate arrays or the SPU must shuffle
AOS data into an SOA form.</li>
<li>Consider the effects of unrolling when choosing an SIMD strategy.</li>
</ul>
</li>
<li><em>Load/Store</em><ul><li>Scalar loads and stores are slow, with long latency.</li>
<li>SPUs only support quadword loads and stores.</li>
<li>Consider making scalars into quadword integer vectors.</li>
<li>Load or store scalar arrays as quadwords, and perform your own extraction
and insertion to eliminate load and store instructions.</li>
</ul>
</li>
<li><em>Branches</em><ul><li>Eliminate nonpredicted branches.</li>
<li>Use feedback-directed optimization.</li>
<li>Use the <samp class="codeph">__builtin_expect</samp> language directive when you
can explicitly direct branch prediction.</li>
</ul>
</li>
<li><em>Multiplies</em><ul><li>Avoid integer multiplies on operands greater than 16 bits in size. The
SPU supports only a <span class="q">"16-bit x16-bit multiply"</span>. A <span class="q">"32-bit multiply"</span> requires
five instructions (three 16-bit multiplies and two adds). </li>
<li>Keep array elements sized to a power-of-2 to avoid multiplies when indexing.</li>
<li>Cast operands to <samp class="codeph">unsigned short</samp> prior to multiplying.
Constants are of type <samp class="codeph">int</samp> and also require casting. Use a
macro to explicitly perform 16-bit multiplies. This can avoid inadvertent
introduction of signed extends and masks due to casting. </li>
</ul>
</li>
<li><em>Pointers</em><ul><li>Use the PPE's  <samp class="codeph">load/store with update</samp> instructions. These
allow sequential indexing through an array without the need of additional
instructions to increment the array pointer. </li>
<li>For the SPEs (which do not support <samp class="codeph">load/store with update</samp> instructions),
use the d-form instructions to specify an immediate offset from a base array
pointer.</li>
</ul>
</li>
<li><em>Dual-Issue</em><ul><li>Choose intrinsics carefully to maximize dual-issue rates or reduce latencies.</li>
<li>Dual issue will occur if a <samp class="codeph">pipe-0</samp> instruction is even-addressed,
a <samp class="codeph">pipe-1</samp> instruction is odd-addressed, and there are no dependencies
(operands are available).</li>
<li>Code generators use <samp class="codeph">nops</samp> to align instructions for dual-issue.</li>
<li>Use software pipeline loops to improve dual-issue rates.</li>
</ul>
</li>
</ul>
</div>
<div>
<div class="familylinks">
<div class="parentlink"><strong>Parent topic:</strong> <a href="cbet_3progspes.html" title="The eight identical Synergistic Processor Elements (SPEs) are optimized for compute-intensive applications in which a program's data and instruction needs can be anticipated and transferred into the local store (LS) by DMA while the SPE computes using previously transferred data and instructions.">Programming the SPEs</a></div>
<div class="previouslink"><strong>Previous topic:</strong> <a href="cbet_3perfanlz.html" title="After a Cell Broadband Engine program executes without errors on the PPE and the SPEs, optimization through parameter-tuning can begin.">Performance analysis</a></div>
</div>
</div></body>
</html>