# ps3linux.net

This is my Playstation 3 Linux website. Hosted on my jailbroke/downgraded Playstation 3 web server via OtherOS (I never turn it off). Distro is Fedora 12 with custom updates built from source.

### Link: [PS3LINUX.NET](http://www.ps3linux.net/~mcps3)

Feedback is welcome; I'm more of a Cell programmer than a web designer (and I'm not really much of a Cell programmer). Anyway, enjoy watching the site progress.

